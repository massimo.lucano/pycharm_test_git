#  Copyright (c) 2022. Droptimize

from engine.common.coordinates import Coordinate


def test_create_coordinate():
    pos = Coordinate(10, 20, 5)

    assert pos.x == 10
    assert pos.y == 20
    assert pos.z == 5

    print(pos)


def test_index():
    pos = Coordinate(x=10, y=20, z=5)
    assert pos[0] == 10
    assert pos[1] == 20
    assert pos[2] == 5


def test_equal():
    a = Coordinate(10, 10, None)
    b = Coordinate(10, None, 4)
    c = Coordinate(10, 20, None)

    assert (a == b) is True  # x match
    assert (a == c) is False  # y doesn't match


def test_add():
    a = Coordinate(10, 10, None)
    b = Coordinate(10, None, None)
    c = a + b
    assert c == Coordinate(20, 10, None)


def test_sub():
    a = Coordinate(10, 10, None)
    b = Coordinate(10, None, None)
    c = a - b
    assert c == Coordinate(0, 10, None)


def test_mul():
    a = Coordinate(10, 10, None)
    c = a * 10
    assert c == Coordinate(100, 100, None)


def test_div():
    a = Coordinate(10, 10, None)
    c = a / 10
    assert c == Coordinate(1, 1, None)


def test_from_list():
    pos = [10, 20, 30]
    a = Coordinate.from_list(pos)
    print(a)

