#  Copyright (c) 2022. Droptimize
import logging
import os
import time
from pathlib import Path

import cv2
import pytest

from engine.common import constants
from engine.modules.imaging.acquisition.acquisition_parameters import AcquisitionParameters
from engine.modules.imaging.imaging import Imaging


logger = logging.getLogger(__name__)


"""
Test all imaging modules features.
"""

CONFIG_PATH = Path(os.environ.get('CONFIG_PATH', './config/'))  # default config path, or taken from env if exist


@pytest.fixture(scope='module')
def imaging() -> Imaging:
    # create module and set config
    imaging = Imaging()
    assert imaging.set_config(CONFIG_PATH / 'ImagingConfig.yaml') is True

    # start all HW connection
    assert imaging.start() is True
    timeout = constants.MODULE_INIT_TIMEOUT
    polling = 0.1
    while timeout > 0 and not imaging.is_ready():
        time.sleep(polling)
        timeout -= polling

    assert imaging.state == Imaging.State.READY

    return imaging


def test_list_cameras(imaging: Imaging):
    """
    List all cameras available with their name
    """
    cam_list = imaging.acquisition.get_camera_infos()  # get infos from all cameras
    assert len(cam_list) > 0  # we expect at least one camera

    cameras = imaging.acquisition.cameras  # get camera object
    assert len(cameras) == len(cam_list)

    for cam in cam_list:
        logger.info(f"Camera: {cam['name']} (trigger: {cam['trigger_name']})")  # exampe of infos
        assert cam['name'] is not None  # check for empty string


def test_get_image(imaging: Imaging):
    """
    Get last image from camera --> used to do the camera calibration

    :param imaging:
    :return:
    """
    cameras = imaging.acquisition.cameras

    time.sleep(2)  # wait a bit to get at least one frame

    for i, c in enumerate(cameras):
        img = c.get_image()
        assert img is not None
        cv2.imwrite('tests/temp/' + f'camera_get_image{i}.bmp', img)


def test_get_resolutions(imaging: Imaging):
    res = imaging.acquisition.cameras[0].get_available_resolutions()
    pass


def test_set_resolution(imaging: Imaging):
    """
    Calibrate camera in both direction (width, height)

    :param imaging:
    :return:
    """
    cameras = imaging.acquisition.cameras

    # get camera (previous test) and find a way to measure a distance on it (um and pixel)
    distance_px = [10.0, 20.0]
    distance_um = [15.0, 30.0]

    resolution = [x/y for x, y in zip(distance_um, distance_px)]

    for cam in cameras:
        cam.set_resolution(resolution[0], resolution[1])


@pytest.mark.parametrize('delay', range(0, 1001, 100))
def test_set_delay(imaging: Imaging, delay):
    """
    Set flashing delay

    :param imaging:
    :return:
    """
    cameras = imaging.acquisition.cameras

    param = AcquisitionParameters(led_delay_us=delay)

    # set all cameras
    #assert imaging.acquisition.set_delay(delay, None, validation_only=True) is True
    assert imaging.acquisition.set_parameters(param, None, validation_only=True)

    # set camera one by one
    for idx, cam in enumerate(cameras):
        assert imaging.acquisition.set_parameters(param, idx, validation_only=True) is True

    # set negative delay
    assert imaging.acquisition.set_parameters(AcquisitionParameters(led_delay_us=-10), None) is False


def test_get_delay(imaging: Imaging):
    assert imaging.acquisition.set_parameters(AcquisitionParameters(led_delay_us=100), camera=None, validation_only=False) is True  # set all cameras
    assert all(param.led_delay_us == 100 for param in imaging.acquisition.get_current_parameters())

    assert imaging.acquisition.set_parameters(AcquisitionParameters(led_delay_us=99), camera=None, validation_only=True) is True  # validation only!
    assert all(param.led_delay_us == 100 for param in imaging.acquisition.get_current_parameters())  # should not change the current value


@pytest.mark.parametrize('frequency', range(0, 1001, 200))
def test_set_jetting_frequency(imaging: Imaging, frequency):
    """
    Set a jetting frequency (trigger and camera only). Please use engine.set_jetting_frequency() instead!

    :param imaging:
    :return:
    """

    # all cameras
    if frequency > 0:
        assert imaging.acquisition.set_parameters(AcquisitionParameters(jetting_frequency=200), camera=None, validation_only=True) is True
    else:
        assert imaging.acquisition.set_parameters(AcquisitionParameters(jetting_frequency=200), camera=None, validation_only=False) is True


def test_get_jetting_frequency(imaging: Imaging):

    assert imaging.acquisition.set_parameters(AcquisitionParameters(jetting_frequency=100), camera=None, validation_only=False) is True
    assert all(param.jetting_frequency == 100 for param in imaging.acquisition.get_current_parameters())

    assert imaging.acquisition.set_parameters(AcquisitionParameters(jetting_frequency=300), camera=None, validation_only=True) is True
    assert all(param.led_delay_us == 100 for param in imaging.acquisition.get_current_parameters())  # should not change the current value


def test_end(imaging):
    imaging.stop()
