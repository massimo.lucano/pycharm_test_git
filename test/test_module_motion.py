#  Copyright (c) 2022. Droptimize

import logging
import os
import time
from pathlib import Path

import numpy as np
import pytest

from engine.common import constants
from engine.common.coordinates import Coordinate
from engine.modules.motion.motion import Motion

logger = logging.getLogger(__name__)

"""
Test all motion modules features. Good starting point to understand how motion module works.
"""

CONFIG_PATH = Path(os.environ.get('CONFIG_PATH', './config/'))  # default config path, or taken from env if exist


@pytest.fixture(scope='module')
def motion() -> Motion:
    # create module and set config
    motion = Motion()
    assert motion.set_config(CONFIG_PATH / 'MotionConfig.yaml') is True

    # start all HW connection
    assert motion.start() is True
    timeout = constants.MODULE_INIT_TIMEOUT
    polling = 0.1
    while timeout > 0 and not motion.is_ready():
        time.sleep(polling)
        timeout -= polling

    assert motion.state == Motion.State.READY

    return motion


def test_calibrate(motion):
    """
    Run calibration procedure (= homing) if any is implemented (HW dependant)
    """
    assert motion.calibrate() is True


def test_get_position(motion):
    """
    Get current position
    """
    p: Coordinate = motion.current_position

    assert p.x is not None  # check if position is valid (axis present and position is known)
    assert all(coord is not None for coord in p)  # test all axis in once


def test_set_position(motion):
    """
    Set the current position
    """
    p = Coordinate(10, 10, 10)  # set all position (x,y,z) in [mm]
    assert motion.set_position(p) is True
    assert motion.current_position == p  # check that position is the one we set

    p = Coordinate(20, None, None)  # set only x position
    motion.set_position(p)
    assert motion.current_position == Coordinate(20, 10, 10)  # y and z should still be at previous position


def test_transformation_matrix(motion):
    """
    Set transformation matrix, in case we want to compensate for any misalignment in the real axis-system (i.e. axis are
    not fully orthogonal, or not scaled)
    # TODO not fully implemented yet
    """
    p = Coordinate(10, 10, 10)  # set all position (x,y,z)
    assert motion.set_position(p) is True
    assert motion.current_position == p

    # transformation matrix: scaling
    trans = [1.0, 2.0, 3.0]
    scaling = [1.5, 0.5, 2.0]
    rot = [0.0, 0.0, 0.0]

    motion.correction_matrix = np.array([[1.0 * scaling[0], 0.0, 0.0, trans[0]],
                                         [0.0, 1.0 * scaling[1], 0.0, trans[1]],
                                         [0.0, 0.0, 1.0 * scaling[2], trans[2]],
                                         [0.0, 0.0, 0.0, 1.0], ])

    assert p == motion.current_position

    p_dest = Coordinate(*[p[i] * scaling[i] + trans[i] for i in range(3)])
    assert Coordinate(*[a.position for a in motion._axis_system[0].axis]) == p_dest


def test_travel_range(motion):
    """
    Read the allowed travel range and speeds
    """
    max = motion.max_position
    min = motion.min_position

    for a, b in zip(min, max):
        assert a < b

    speed = motion.max_speed
    for s in speed:
        assert s > 0


def test_absolute_movement(motion):
    """
    Make absolute movement by using motion.move_to() method
    """
    p = Coordinate(0, 0, 0)
    motion.set_position(p)

    speed = 1.0
    distance = 10.0
    assert motion.set_speed(Coordinate(speed, speed, speed)) is True

    start = time.time()
    dest = Coordinate(distance, distance, distance)
    assert motion.move_to(dest) is True
    assert motion.is_moving == Coordinate(True, True, True)  # is_moving should be True as soon as move_to returns

    timeout_secs = distance / speed * 2
    polling = 0.1
    #  is_moving stays True while axis is in motion
    while timeout_secs > 0 and any(motion.is_moving):
        time.sleep(polling)
        timeout_secs -= polling

    assert timeout_secs > 0
    elapsed = time.time() - start

    assert distance / speed * 0.8 < elapsed < distance / speed * 1.2  # check travel time

    assert motion.current_position == dest  # check target position


def test_relative_movement(motion):
    """
    Make relative movement by using motion.move_by() method.
    """
    start_pos = motion.current_position

    assert motion.is_moving == Coordinate(False, False, False)

    speed = 1.0
    distance = 10.0

    start = time.time()
    offset = Coordinate(distance, distance, distance)
    assert motion.move_by(offset, speed=Coordinate(speed, speed, speed)) is True
    assert motion.is_moving == Coordinate(True, True, True)  # is_moving should be True as soon as move_to returns

    timeout_secs = distance / speed * 3
    polling = 0.1
    #  is_moving stays True while axis is in motion
    while timeout_secs > 0 and any(motion.is_moving):
        time.sleep(polling)
        timeout_secs -= polling
        # print(motion.current_position)

    assert timeout_secs > 0
    elapsed = time.time() - start

    assert distance / speed * 0.8 < elapsed < distance / speed * 1.2  # check travel time

    assert motion.current_position == start_pos + offset  # check target position


def test_moving_flag(motion):
    """
    Check usage of moving flag
    """

    # reset position
    p = Coordinate(0, 0, 0)
    motion.set_position(p)

    # set speed
    speed = 1.0
    assert motion.set_speed(Coordinate(speed, speed, speed)) is True
    assert any(motion.is_moving) is False

    # make one movement on each axis (one after the other) and check that:
    # - motion.is_moving flag is set for the moving axis
    # - travel time is realistic
    # - target position is reached
    distance = 2.0
    dest_list = [Coordinate(distance, None, None), Coordinate(None, distance, None), Coordinate(None, None, distance)]
    for dest in dest_list:
        start = time.time()

        assert motion.move_to(dest) is True
        assert any(motion.is_moving) is True  # is_moving should be True as soon as move_to returns

        timeout_secs = distance / speed * 2
        polling = 0.1
        #  is_moving stays True while axis is in motion
        while timeout_secs > 0 and any(motion.is_moving):
            time.sleep(polling)
            timeout_secs -= polling

        assert timeout_secs > 0
        elapsed = time.time() - start

        assert distance / speed * 0.5 < elapsed < distance / speed * 2.0  # check travel time

        assert any(motion.is_moving) is False
        assert motion.current_position == dest  # check target position


def test_stop(motion):
    """
    Stop axis. Abort current movement if any
    """
    p = Coordinate(0, 0, 0)
    motion.set_position(p)

    speed = 1.0
    distance = 10.0

    start = time.time()
    speed2 = Coordinate(speed, speed, speed)
    dest = Coordinate(distance, distance, distance)
    assert motion.move_to(dest, speed2) is True
    assert motion.is_moving == Coordinate(True, True, True)  # is_moving should be True as soon as move_to returns
    print(motion.current_position)

    timeout_secs = distance / speed * 2
    polling = 0.1
    #  is_moving stays True while axis is in motion
    while timeout_secs > 0 and any(motion.is_moving):
        time.sleep(polling)
        timeout_secs -= polling
        print(motion.current_position)
        motion.move_abort()

    assert timeout_secs > 0
    elapsed = time.time() - start

    assert elapsed < distance / speed * 1.2  # check travel time

    assert motion.current_position != dest  # check target position


def test_stop_module_motion(motion):
    assert motion.stop() is True
    assert motion.state == Motion.State.IDLE


def test_restart_module_motion(motion):
    assert motion.start() is True
    assert motion.state == Motion.State.READY


def test_restop_module_motion(motion):
    assert motion.stop() is True
    assert motion.state == Motion.State.IDLE
