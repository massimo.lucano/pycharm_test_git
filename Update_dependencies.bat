rem CONFIG PIP.INI to install dependencies manually
rem python -m pip config set global.index-url https://ci_cd_gitlab_api:glpat-dPC-xxQxeybcmry69pPC@gitlab.forge.hefr.ch/api/v4/groups/2420/-/packages/pypi/simple
rem python -m pip config set global.index https://ci_cd_gitlab_api:glpat-dPC-xxQxeybcmry69pPC@gitlab.forge.hefr.ch/api/v4/groups/2420/-/packages/pypi/
rem python -m pip config set global.trusted-host gitlab.forge.hefr.ch


rem Create Virtual Environment called "env" and activate it
py -m pip install --user virtualenv
py -m venv env
@echo off
CALL .\env\Scripts\activate

rem Install dependencies described in pyproject.toml
pip install --index-url https://ci_cd_gitlab_api:glpat-dPC-xxQxeybcmry69pPC@gitlab.forge.hefr.ch/api/v4/groups/2420/-/packages/pypi/simple --upgrade ".[dev]"
rem pip install --index-url https://ci_cd_gitlab_api:glpat-dPC-xxQxeybcmry69pPC@gitlab.forge.hefr.ch/api/v4/groups/2420/-/packages/pypi/simple --upgrade .
rem pre-commit install --hook-type commit-msg

rem Copy downloaded githooks
copy .\env\Lib\site-packages\githooks\post-commit .\.git\hooks\post-commit
copy .\env\Lib\site-packages\githooks\commit-msg .\.git\hooks\commit-msg
