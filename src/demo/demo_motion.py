#  Copyright (c) 2022. Droptimize

import os
import time
from pathlib import Path

from engine.common import constants
from engine.common.coordinates import Coordinate
from engine.modules.motion.motion import Motion

CONFIG_PATH = Path(os.environ.get('CONFIG_PATH', './config/'))  # default config path, or taken from env if exist


if __name__ == '__main__':

    # get the "motion" module
    motion = Motion()
    motion.set_config(CONFIG_PATH / 'MotionConfig.yaml')

    # connect to all HW and start all monitoring threads
    motion.start()

    # wait until ready
    timeout = constants.MODULE_INIT_TIMEOUT
    polling = 0.1
    while timeout > 0 and not motion.is_ready():
        time.sleep(polling)
        timeout -= polling

    # Check its state
    motion.state.name

    # Get the min/max positions and speed
    motion.max_speed
    motion.min_position
    motion.max_position

    # Check if already homed
    motion.is_calibrated

    # Do the homing of the axis
    motion.calibrate()

    # Get the current position
    motion.current_position

    #  Move to, on one axis
    p = Coordinate(x=12)
    motion.move_to(p)

    # Multiple axis with coordinated movement
    p = Coordinate(x=15, y=12)
    motion.move_to(p)

    # Overwrite the default speed
    p = Coordinate(x=15) # Target position in mm
    s = Coordinate(x=1) # Speed in mm/s (default max speed)
    motion.move_to(p, s)

    # Check if the axis is still moving
    motion.is_moving

    # Move to a relative (instead of abs) position
    p = Coordinate(x=10)  # Target position in mm
    s = Coordinate(x=1)  # Speed in mm/s (default max speed)
    motion.move_by(p, s)

    # overwrite the current position
    p = Coordinate(x=0)  # Target position in mm
    motion.set_position(p)

    # Overwrite the default speed (max speed)
    s = Coordinate(x=50)  # Target speed in mm/s
    motion.set_speed(s)

    # Abort an ongoing motion
    motion.move_abort()

    # Emergency stop from the software to stop the motion
    motion.emergency_stop()

    # Stop the motion module
    motion.stop()
