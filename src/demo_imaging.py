#  Copyright (c) 2022. Droptimize

import os
import time
from pathlib import Path

from engine.common import constants
from engine.modules.imaging.acquisition.acquisition_parameters import AcquisitionParameters
from engine.modules.imaging.imaging import Imaging

CONFIG_PATH = Path(os.environ.get('CONFIG_PATH', 'config/'))  # default config path, or taken from env if exist

if __name__ == '__main__':
    # get the "imaging" module
    imaging = Imaging()
    imaging.set_config(CONFIG_PATH / 'ImagingConfig.yaml')

    # connect to all HW
    imaging.start()

    # wait until ready
    timeout = constants.MODULE_INIT_TIMEOUT
    polling = 0.1
    while timeout > 0 and not imaging.is_ready():
        time.sleep(polling)
        timeout -= polling
        print(f'{timeout}')

    """
    # commente out by MPL; replace with code for printing
    # execute only if configuration       iprint-fpga:  frequency_master: True
    
    # prepare acquisition parameters (i.e. flash delay, duration, ...)
    param = AcquisitionParameters(led_delay_us=10, led_duration_us=0.5, jetting_frequency=1000)  # all the specified parameters will be set together. The others are ignored (= not updated)

    # send to all cameras/triggers together
    imaging.acquisition.set_parameters(param)

    # start generating some pulse for LED and camera (= acquiring some image). But without printing
    imaging.acquisition.start_trigger(print_enable=False)

    # stop trigger
    imaging.acquisition.stop_trigger()

    # start again, but with sending pulses to print system too (= jetting). And only with camera 0
    imaging.acquisition.start_trigger(camera=0, print_enable=True)

    # stop jetting
    imaging.acquisition.stop_trigger()
    """
    # stop module
    imaging.stop()
