#  Copyright (c) 2022. Droptimize

import logging
import time
from threading import Thread, Event, RLock

logger = logging.getLogger(__name__)


class Watchdog:
    """
    Watchdog template. All method are thread-safe
    """

    _WORKER_PERIOD = 0.05

    def __init__(self, timeout_handler, polling_period, name):
        """
        Create a watchdog.

        :param timeout_handler: method to call if watchdog event occurs. Should be thread-safe
        :param polling_period:
        :param name:
        """
        if polling_period < 5*self._WORKER_PERIOD:
            polling_period = 5*self._WORKER_PERIOD
            logger.warning(f"Watchdog period too small, set to minimum period of {polling_period}s")
        self._polling_period = polling_period
        self._timeout_handler = timeout_handler
        self._name = name
        self._thread = None
        self._stop_event = Event()

        self._lock = RLock()  # for remaining_time variable
        self._remaining_time = polling_period

    def __del__(self):
        self.stop()

    @property
    def is_started(self):
        return self._thread is not None and self._thread.is_alive()

    def start(self):
        """
        Start the specified timer

        :return: True if successfully started
        """
        if self._thread is not None:
            if self._thread.is_alive():
                logger.error("Impossible to start watchdog as it's already running")
                return False
            else:
                self._thread.join()

        with self._lock:
            self._remaining_time = self._polling_period

        self._thread = Thread(target=self._watchdog, name=self._name)
        self._thread.start()

        return True

    def stop(self):
        """
        Stop the watchdog

        :return:
        """
        if self._thread is not None:
            self._stop_event.set()
            self._thread.join()
            self._stop_event.clear()
            self._thread = None
        return True

    def reset(self):
        """
        Period reset to call to say all is fine.

        :return: True if successfully resetted
        """
        if not self.is_started:
            logger.warning("Cannot reset watchdog if not started. Ignoring")
            return False

        with self._lock:
            self._remaining_time = self._polling_period
        return True

    def _watchdog(self):
        while not self._stop_event.is_set():
            time.sleep(self._WORKER_PERIOD)
            with self._lock:
                self._remaining_time -= self._WORKER_PERIOD

            if self._remaining_time <= 0 and not self._stop_event.is_set():
                logger.warning("Watchdog triggered")
                self._timeout_handler()
                return 0

        return 0
