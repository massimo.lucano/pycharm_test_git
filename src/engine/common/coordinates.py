#  Copyright (c) 2022. Droptimize

import numbers


class Coordinate:
    """
    Axis convention: please refer to the documentation
    """
    def __init__(self, x=None, y=None, z=None):
        self.x = x
        self.y = y
        self.z = z

    def __composite_values__(self):
        return self.x, self.y, self.z

    def __ne__(self, c):
        return not self.__eq__(c)

    def __eq__(self, c):
        eq = True

        if None not in [self.x, c.x] and (self.x != c.x):
            eq = False
        if None not in [self.y, c.y] and (self.y != c.y):
            eq = False
        if None not in [self.z, c.z] and (self.z != c.z):
            eq = False

        return eq

    def __str__(self):
        str = ''
        for n, v in zip('xyz', list(self)):
            str += f'{n}:'
            if v is None:
                str += 'None '
            else:
                str += f'{v:.3f} '
        return str

    def __repr__(self):
        return f'Coordinate(x={self.x}, y={self.y}, z={self.z})'

    def __copy__(self):
        return type(self)(self.x, self.y, self.z)

    def __add__(self, c):
        """
        Add two Coordinate object. If None added to a real value = no change. None + None = None
        :param c:
        :return:
        """
        if isinstance(c, type(self)):
            ret = Coordinate(*[sum([v for v in [a, c] if v is not None]) if (a is not None) or (c is not None) else None for a, c in zip(list(self), c)])
            return ret
        else:
            raise NotImplemented

    def __sub__(self, c):
        if isinstance(c, type(self)):
            ret = Coordinate()
            if self.x is not None and c.x is not None:
                ret.x = self.x - c.x
            if self.y is not None and c.y is not None:
                ret.y = self.y - c.y
            if self.z is not None and c.z is not None:
                ret.z = self.z - c.z
            return ret
        else:
            raise NotImplemented

    def __mul__(self, other):
        if isinstance(other, numbers.Number):
            return type(self)(self.x * other if self.x is not None else None,
                              self.y * other if self.y is not None else None,
                              self.z * other if self.z is not None else None)
        else:
            return NotImplemented

    def __truediv__(self, other):
        if isinstance(other, numbers.Number):
            return type(self)(self.x / other if self.x is not None else None,
                              self.y / other if self.y is not None else None,
                              self.z / other if self.z is not None else None)
        else:
            return NotImplemented

    def __iter__(self):
        for i in [self.x, self.y, self.z]:
            yield i

    def __getitem__(self, index):
        return list(self)[index]

    def __setitem__(self, index, value):
        if index == 0:
            self.x = value
        elif index == 1:
            self.y = value
        elif index == 2:
            self.z = value
        else:
            raise IndexError

    @classmethod
    def from_list(cls, li):  # todo make a native ctor for that?
        return cls(li[0], li[1], li[2])
