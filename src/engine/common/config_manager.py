#  Copyright (c) 2022. Droptimize

# instanciate by each object using config file.

# should also contains all the allowed keys / used keys of this object (help to create YAML definition!)

# manage access errors, parsing if required, ...

# should also manage config update?

# FEATURE define a pattern for config / config changes. Either the config triggers the modules (publish/subscribe), or the modules read again the config by themselves (tree update from single command).
#  -> go for publish/subscribe if possible!


# ConfigManager are created by modules (one per config file). They are then transferred to submodules (which can creates sub configmanager?)
import logging
from multiprocessing import RLock
from typing import List

import yaml

logger = logging.getLogger(__name__)


class ConfigManager:
    """
    Manage config file (checks, key access, etc.)
    Thread-safe class.
    """
    VERSION = '1.0.0'

    # DB

    def __init__(self, root_name: str):
        # super().__init__()
        self._version = self.VERSION
        self._root_name = root_name  # name of the current category
        self._config = {}  # config files
        self._required_keys = []

        self._parent_config = None
        self._config_lock = RLock()

    @property
    def is_valid(self):
        """
        Check if the config is valid

        :return: True is valid, False otherwise
        """
        return self._config != {}

    def get_root_path(self):
        """
        Return the path of the current config object from its topmost parent
        :return:
        """
        with self._config_lock:
            path = ''
            if self._parent_config:
                path += self._parent_config.get_root_path() + ','
            path += self._root_name
        return path

    def add_required_key(self, keys: List[str]):
        """
        Specify the required properties for a module
        :param keys: list of keys to check (only at first level, no subkeys)
        :return:
        """
        with self._config_lock:
            for k in keys:
                self._required_keys.append(k)  # todo switch to set instead of list

    def get_property(self, property_name):
        """

        :param property_name: list of key or indexes as path to the property (even if only one)
        :return: the value or None if nothing found
        """
        if not isinstance(property_name, list):
            prop = [property_name]
        else:
            prop = property_name
        # prop = list(property_name)

        with self._config_lock:
            # if all(p in self._config.keys() for p in property_name):
            v = self._config
            for n in prop:
                if isinstance(v, list) or (n in v.keys()):
                    v = v[n]
                else:
                    j = ''.join([str(elem) for elem in prop])
                    # logger.debug(f'Cannot find {j} in config {self.get_root_path()}')
                    return None
            return v

    def set_config_from_parent(self, parent_config, root_key):
        with self._config_lock:
            self._parent_config = parent_config
            subconfig = parent_config.get_property(root_key)
            if self._check_if_all_keys(self._required_keys,
                                       subconfig.keys()):  # all(key in subconfig.keys() for key in self._required_keys):
                self._config = subconfig
                return True
            else:
                logger.error(f"Not all keys are present for the config: {self.get_root_path()}")
                return False

    def set_config_file(self, config, root_key):
        """
        Parse config file and check if all the required fields are available
        :param config: filename
        :param root_key: the top key
        :return: True if config is valid, False otherwise
        """
        if config is not None:
            with self._config_lock:
                try:
                    with open(config, 'r') as stream:
                        try:
                            conf = yaml.safe_load(stream)
                            conf = conf[root_key]
                        except yaml.YAMLError as exc:
                            logger.error(f"Error while loading YAML config file: {exc}")
                            return None
                except IOError:
                    logger.error("File not accessible" + str(config))
                    return False
                except KeyError:
                    logger.error("Root key not found" + root_key)
                    return False

                # conf is parsed here, check all keys (at single level) present before we save it as new reference
                if self._check_if_all_keys(self._required_keys,
                                           conf.keys()):  # all(key in conf for key in self._required_keys):
                    self._config = conf
                    self._parent_config = None
                    return True
                else:
                    logger.error(f"Not all keys are present for the config: {self.get_root_path()}")
                    return False
        else:
            return False

    def _check_if_all_keys(self, required_keys, current_keys):
        required_keys = set(required_keys)
        current_keys = set(current_keys)

        if current_keys.issuperset(required_keys):
            if current_keys != required_keys:
                logger.info(f"Extra unknown config keys: {','.join(current_keys - required_keys)}")  # extra keys found
            return True
        else:
            logger.error(f"Missing keys: {','.join(required_keys - current_keys)}")
            return False

    def find_key(self, key):
        """
        Find a specific key in the whole dictionnary

        Note: Not tested.

        :param key:
        :return:
        """
        with self._config_lock:
            for k, v in self._config.items():
                if k == key:
                    yield v
                elif isinstance(v, dict):
                    for result in self.find_key(v):
                        yield result
                elif isinstance(v, list):
                    for d in v:
                        for result in self.find_key(d):
                            yield result
