
#  Copyright (c) 2022. Droptimize

MODULE_INIT_TIMEOUT = 60.0
MODULE_LOCK_TIMEOUT = 0.1
MAX_TRAVEL_DURATION = 30.0  # max time to wait for a movement to end in seconds
