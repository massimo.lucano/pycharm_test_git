"""
.. module:: module
   :synopsis: Contain abstract class Module.
.. moduleauthor:: Florian Bourguet <florian@droptimize.ch>
"""

#  Copyright (c) 2022. Droptimize

# Class Module defining common features for all modules
import logging
import threading
from abc import abstractmethod

from enum import IntEnum

from engine.common.config_manager import ConfigManager

logger = logging.getLogger(__name__)


class Module:
    """
    Abstract class as common basis for all modules

    Each sub-class of Module must follow the Modules Guidelines:

    1. The module config as well as any other configuration can be updated at any time (in between tasks) and should
       remain state-independent.
    2. The module should not instantiate/communicate with more than one HAL (= one direct HW link).
    3. If the module is creating additional thread, they should not be start before we call the start() method of
        the module, and they should terminate during the call to stop() method.
    4. All methods should return immediately. If it implies longer task. They should start in their own thread and
        provide a way to the user to poll/get notified for its status.
    5. Module can be called by multiple client at the same time. All methods should be either thread-safe or implement
        thread-safe mechanism if required.
    6. The module can cache HW access if it can be guaranteed that the data are correct and not changed by other ways.

    TODO More rules to be defined
    """

    # States enum
    class State(IntEnum):
        UNKNOWN = 0
        IDLE = 1
        INIT = 2
        READY = 3
        BUSY = 4
        ERROR = 5

    def __init__(self, name: str, version: str):
        self._state = self.State.IDLE
        self._version = version
        self._name = name

        self._module_lock = threading.RLock()  # common lock to protect against concurrent access
        self._module_config = ConfigManager(
            name)  # todo : implies that every module has a different config file, not very nice.

    @property
    def version(self) -> str:
        """
        Get Module version

        :return: version as string
        """
        return self._version

    @property
    def name(self) -> str:
        """
        Get Module name

        :return: name as string
        """
        return self._name

    @property
    def state(self):
        """
        Get Module status

        :return: state as string
        """
        return self._state

    def is_ready(self) -> bool:
        """
        Check if module is ready to use
        Module should be called only when in ready state.

        :return: True if ready. False otherwise.
        """
        return self._state == self.State.READY

    def set_config(self, module_config: str):
        """
        (Re-)configure module.
        Set all static parameters (= not related to a function call).

        :param module_config: path to the config file
        :return: True if config was successfully read and applied, False otherwise
        """
        if self._state == self.State.IDLE:
            with self._module_lock:
                logger.info(f'Update module {self._name} with config file {module_config}')

                if self._module_config.set_config_file(module_config, self.__class__.__name__.lower()):  # parse file and check if all required fields are here
                    self._update_module_config()  # if yes, apply the new config # todo we do not check return value here: auto revert if not accepted?
                    self._state = self.State.IDLE
                    return True
                else:
                    logger.error(f'Error while setting module config : {module_config}')
                    self._state = self.State.ERROR
                    return False
        else:
            logger.error("Cannot set module config if not in Idle state")
            return False

    def start(self) -> bool:
        """
        Start the module. If it fails, stop it again.
        If the module is already started, ignore it.

        :return: True if the module is started without error, False otherwise.
        """
        if self._state == self.State.READY:
            logger.warning("Module already running, ignoring start request")
            return True
        else:
            with self._module_lock:
                if not self._module_config.is_valid or self._state is not self.State.IDLE:
                    logger.error("Cannot start module if config is not valid and not in IDLE state")
                    return False

                # start interface
                result = self._start()  # is True

                if result:
                    if result:
                        self._state = self.State.READY
                    else:
                        self._stop()

                return result

    @abstractmethod
    def _start(self) -> bool:
        """
        Module-dependant start commands. To be implemented.

        :return: True if the module is started without error, False otherwise.
        """
        raise NotImplementedError

    def stop(self) -> bool:
        """
        Stop the module.

        :return: True is module was successfully shutdown, False otherwise.
        """
        if self._state is self.State.IDLE:
            logger.warning("Module already in IDLE state, ignoring stop request")
            return True
        else:
            with self._module_lock:
                self._state = self.State.IDLE

                return self._stop()  # is True

    @abstractmethod
    def _stop(self) -> bool:
        """
        Module-dependant start commands. To be implemented.

        :return: True is module was successfully shutdown, False otherwise.
        """
        raise NotImplementedError

    @abstractmethod
    def emergency_stop(self):
        """
        Emergency stop. Try to stop hardware as soon as possible. Recovering from Estop state require to stop the module
        ATTENTION : make this method return immediately in all cases.
        :return: Immediately with True.
        """
        raise NotImplementedError

    @abstractmethod
    def _update_module_config(self):
        """
        Read the config, check it and apply it if valid

        :return: True if config is accepted, False otherwise # todo return not implemented in all modules
        """
        raise NotImplementedError("Please Implement this method")
