import logging
import threading
from engine.modules.imaging.acquisition.acquisition import Acquisition
from engine.modules.module import Module

logger = logging.getLogger(__name__)


class Imaging(Module):
    """
    Connect to a camera and a trigger. Acquires the frames and perform the image analysis. Provides basic data extraction from the raw values.

    """
    _VERSION = '0.1'

    def __init__(self):
        super().__init__('Imaging Module', self._VERSION)
        self._acquisition = Acquisition()
        modules_keys = [Acquisition.CONFIG_PATH]
        self._module_config.add_required_key(modules_keys)

        self._abort_acquisition = threading.Event()

    @property
    def acquisition(self):
        """
        Current acquisition

        :return:
        """
        return self._acquisition

    def _start(self):
        """
        Start everything to be ready to acquire and analyse images

        :return: True if everything is ready, False otherwise
        """
        ret = self._acquisition.connect()
        if ret:
            self._state = self.State.READY

        return ret

    def _stop(self):
        """
        Stop all components

        :return: True if successfully stopped, False otherwise
        """
        return self._acquisition.disconnect()

    def emergency_stop(self):
        """
        Emergency stop - stop analyzer to free resources and reaction time.
        :return:
        """
        return True

    def _update_module_config(self):  # todo manage return values from set_config() -> clean that better
        # Acquisition
        if not self._acquisition.set_config(self._module_config, [Acquisition.CONFIG_PATH]):
            return False

        return True
