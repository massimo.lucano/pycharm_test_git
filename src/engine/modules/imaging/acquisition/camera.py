import logging
import math
from abc import ABC, abstractmethod
from typing import Optional, Tuple

from engine.common.config_manager import ConfigManager
from engine.modules.imaging.acquisition.acquisition_parameters import AcquisitionParameters
from engine.modules.imaging.acquisition.trigger import Trigger

try:
    from engine.modules.imaging.acquisition.acquisition import Acquisition
except ImportError:
    pass  # skip circular import second pass

logger = logging.getLogger(__name__)


class Camera(ABC):
    """
    Base class for cameras.

    All cameras should provide the same interface by extending this class and implementing its abstract methods.

    It's up to the camera to ask the trigger to update it's config (cam/led delay and duration)

    Camera should implement an interface to configure, open and close the camera link.
    Images for analysis are accessed via the get_image() method.
    For preview, the camera should implement a Gstreamer 'intervideosink' with a unique name (given by the config file)

    """
    CONFIG_PATH = 'cameras'
    _camera_count = 0

    class Led:
        CONFIG_PATH = 'led'

        def __init__(self):
            self._config = ConfigManager(self.CONFIG_PATH)
            self._config.add_required_key(
                ['enable', 'max_duty_cycle', 'max_on_time_us', 'trigger_idx', 'trigger_idx_2'])

        def set_config(self, config_manager, root_key):
            return self._config.set_config_from_parent(config_manager, root_key)

        def check_for_safe_conditions(self, frequency, duration_us):
            """
            Check if ON time is safe for the LED
            :param frequency: flash frequency
            :param duration_us: flash duration in us
            :return: True if safe, False if overloaded
            """
            if frequency is None or duration_us is None:
                logger.warning("Missing information to check properly LED safe conditions")
                return False

            if duration_us > self._config.get_property('max_on_time_us'):
                logger.warning(f"Required parameters exceed LED safe conditions (max on time) : {duration_us}us")
                return False

            duty_cycle = duration_us * (frequency * 1e6)
            if duty_cycle > self._config.get_property('max_duty_cycle'):
                logger.warning(f"Required parameters exceed LED safe conditions (max duty cycle) : {duty_cycle}%")
                return False

            return True

        @property
        def trigger_idx(self):
            # if self._trigger:
            #    range = self._trigger.get_cam_idx_range()
            idx = self._config.get_property(['trigger_idx'])
            return int(idx)

        @property
        def trigger_idx_double_strobe(self):
            # if self._trigger:
            #    range = self._trigger.get_cam_idx_range()
            idx = self._config.get_property(['trigger_idx_2'])
            return int(idx)

        @property
        def max_led_on_time_us(self):
            return self._config.get_property('max_on_time_us')

    def __init__(self):
        super().__init__()
        Camera._camera_count += 1  # increment instance counter

        self._config = ConfigManager(self.CONFIG_PATH)
        self._config.add_required_key(
            ['trigger', 'trigger_idx', 'look_at', 'shutter_predelay_us', 'shutter_postdelay_us',
             'shutter_min_duration_us',
             'fps_max', 'led', 'resolution_x', 'resolution_y', 'um_per_px'])
        self._trigger: Trigger = None  # attached trigger
        self._led = self.Led()

        self._resolution_um_per_px = [1.0, 1.0]  # width (x), height (y)

        self._current_cam_frame_interval = None  # cache this value as it will be mixed with max_fps in camera_divider value

    @property
    def trigger_idx(self):
        # TODO check valid range (via trigger config?)
        # if self._trigger:
        #    range = self._trigger.get_cam_idx_range()
        idx = self._config.get_property(['trigger_idx'])
        return int(idx)

    @property
    def led(self):
        return self._led

    @property
    def image_size_px(self):
        return self._config.get_property('resolution_x'), self._config.get_property('resolution_y')

    @property
    def look_at_vector(self):
        return self._config.get_property('look_at')

    def set_config(self, config_manager, root_key):
        if self._config.set_config_from_parent(config_manager, root_key):
            # set default resolution from config
            self._resolution_um_per_px = [float(self._config.get_property('um_per_px')),
                                          float(self._config.get_property('um_per_px'))]

            if self._led.set_config(self._config, self.Led.CONFIG_PATH):  # configure LED as well
                return self.configure()
            else:
                return False
        else:
            return False

    def set_resolution(self, um_per_px_x, um_per_px_y):
        """
        Set the resolution in width and height

        :param um_per_px_x: calibration value, if None, not taken into account
        :param um_per_px_y: calibration value, if None, not taken into account
        :return:
        """
        logger.info(f"Set camera resolution: {um_per_px_x}/{um_per_px_y}")

        if um_per_px_x is not None and um_per_px_x > 0:
            self._resolution_um_per_px[0] = um_per_px_x

        if um_per_px_y is not None and um_per_px_y > 0:
            self._resolution_um_per_px[1] = um_per_px_y

        return True

    def _update_camera_divider(self, frequency, cam_duration_us, cam_frame_interval=None, validation_only=False):
        """
        Set camera frequency to reach max_fps or given frame interval if any specified

        :param frequency: current jetting frequency
        :param validation_only: if True, check only if the value is valid or not
        :return: True if divider is valid and applied, False otherwise
        """
        # compute divider according to max cam FPS
        try:
            worst_cam_duration = 1 / self.get_max_fps()
            if cam_frame_interval:
                if cam_frame_interval < worst_cam_duration:
                    logger.warning(
                        f"Cannot set frame interval of {cam_frame_interval}s because resulting FPS ({1 / cam_frame_interval}) > camera max. framerate ({self.get_max_fps()})")
                else:
                    worst_cam_duration = cam_frame_interval  # reduce cam framerate to match cam_frame_interval
            divider = math.ceil(frequency * worst_cam_duration)
        except ZeroDivisionError:
            return False

        # check if camera is not over-triggered
        total_cam_time_us = cam_duration_us
        if not self._trigger.support_parallel_trigger:
            # check if 2nd trigger can start while previous one is still running (delay superimposed to prev. duration)
            # TODO how many trigger can start in parallel instead! (i.e 10 for DroptimizeTrigger)
            total_cam_time_us += self._trigger.get_camera_delay(self)

        cam_trigger_period_us = 1e6 / frequency * divider
        if total_cam_time_us > cam_trigger_period_us:
            logger.info(
                f"Camera is over-triggered (exposure time: {total_cam_time_us}us > cam. trigger period: {cam_trigger_period_us}us), reducing framerate")
            divider = math.ceil(frequency * total_cam_time_us * 1e-6)

        if divider <= 0:
            return False
        else:
            logger.info(f"Set camera divider to {divider} for frequency {frequency}")
            return self.trigger.set_camera_divider(self, divider, validation_only)

    def configure(self):
        """
        Configure all camera-specific parts before staring the camera (i.e. setup pipeline)

        :return: True if configuration is successful, False otherwise
        """
        ret = self._configure()

        return ret

    def start_trigger(self, cam_frames=None, print_enable=True):
        """
        Start trigger for X frames
        :param cam_frames:
        :param print_enable: True if the print system should also receive the trigger pulses
        :return:
        """
        return self._trigger.start_acquisition(cam_frames, self, print_enable)

    def stop_trigger(self):
        """
        Stop the trigger
        """
        return self._trigger.stop_acquisition()

    @abstractmethod
    def _configure(self):
        """
        Configure all camera-specific parts before staring the camera (i.e. setup pipeline)

        :return: True if configuration is successful, False otherwise
        """
        raise NotImplementedError

    @property
    def name(self):
        """
        Name of the camera
        :return:
        """
        if self._config:
            return self._config.get_property(['name'])
        else:
            return None

    @property
    def trigger_name(self) -> str:
        """
        Name of the trigger
        :return:
        """
        return self._config.get_property(['trigger'])

    @property
    def trigger(self) -> Optional[Trigger]:
        """
        Reference to the trigger
        :return:
        """
        return self._trigger

    @trigger.setter
    def trigger(self, ref):
        self._trigger = ref

    # todo manage illumination (leds)

    @abstractmethod
    def open(self) -> bool:
        """
        Open HW connection

        :return: True if succeeded, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def close(self) -> bool:
        """
        Close HW connection

        :return: True if succeeded, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def enable_freewheel(self, fps=None):
        """

        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def disable_freewheel(self):
        """

        """
        raise NotImplementedError("Please Implement this method")

    def set_parameters(self, param: AcquisitionParameters, validation_only=False):
        """
        Set camera parameter(s) if valid.
        This method check if the parameter is not valid. HW can then also recheck if the value is out of it's range.

        :param param: Acquisition.Parameters object
        :param validation_only: if True, check only if the value is valid or not
        :return: True if the parameter is set successfully, False otherwise
        """
        update_cam_exposure_duration = False  # indicate whether any other parameter is influencing the exposure time

        res = True
        if param.led_delay_us:
            if param.led_delay_us >= self._config.get_property(['shutter_predelay_us']):
                cam_delay = param.led_delay_us - self._config.get_property(['shutter_predelay_us'])
                logger.debug(f"Set led delay to {param.led_delay_us}us and cam delay to {cam_delay}us")
                res &= self._trigger.set_camera_delay(self, cam_delay, validation_only)
                res &= self._trigger.set_flash_delay(self, param.led_delay_us, validation_only)
                update_cam_exposure_duration = True
            else:
                logger.error(
                    f'Invalid flash delay {param.led_delay_us} us. Min is {self._config.get_property(["shutter_predelay_us"])} us')
                res = False

        if param.led_duration_us:
            res &= (param.led_duration_us > 0) and self._trigger.set_flash_duration(self, param.led_duration_us,
                                                                                    validation_only)
            update_cam_exposure_duration = True

        if param.double_strobe_delay_us:
            logger.debug(f"Set double strobe delay to {param.double_strobe_delay_us}us")
            res &= (param.double_strobe_delay_us > 0) and self._trigger.set_double_strobe_delay(self,
                                                                                                param.double_strobe_delay_us,
                                                                                                validation_only)
            update_cam_exposure_duration = True

        if param.drops_per_img:
            logger.debug(f"Set drops per image to {param.drops_per_img}")
            # drops per image is done by extending open time of camera shutter and flashing all drops in the meantime.
            self._trigger.set_drops_per_img(self, param.drops_per_img, validation_only)  # save value for future use
            update_cam_exposure_duration = True

        if param.cam_frame_interval:
            logger.debug(f"Set frame interval to {param.cam_frame_interval} drops")
            self._current_cam_frame_interval = param.cam_frame_interval
            update_cam_exposure_duration = True

        if param.jetting_frequency:
            logger.debug(f"Set camera jetting frequency to {param.jetting_frequency}Hz")
            res &= (param.jetting_frequency > 0) and self._trigger.set_jetting_frequency(param.jetting_frequency,
                                                                                         validation_only)
            update_cam_exposure_duration = True  # in case drops_per_img > 1

            # if not update_cam_exposure_duration:
            #     res &= self._update_camera_divider(param.jetting_frequency, self._trigger.get_camera_duration(self),
            #                                        self._trigger.get_frame_interval(self), validation_only)

        # update camera exposure time
        if update_cam_exposure_duration:
            drops_per_img = max(
                [1, param.drops_per_img if param.drops_per_img else self._trigger.get_drops_per_img(self)])
            frequency = param.jetting_frequency if param.jetting_frequency else self._trigger.get_jetting_frequency()

            jetting_period_us = 1_000_000 / frequency
            cam_duration = self._config.get_property(['shutter_predelay_us']) \
                           + (self._trigger.get_flash_duration(self) + self._trigger.get_double_strobe_delay(self)) \
                           + (drops_per_img - 1) * jetting_period_us \
                           + self._config.get_property(['shutter_postdelay_us'])

            if cam_duration < self._config.get_property('shutter_min_duration_us'):
                cam_duration = self._config.get_property('shutter_min_duration_us')

            logger.debug(f"Set camera exposure time to {cam_duration}us")
            res &= self._trigger.set_camera_duration(self, cam_duration, validation_only)
            res &= self._set_exposure_time(cam_duration, validation_only)

            res &= self._update_camera_divider(frequency, self._trigger.get_camera_duration(self),
                                               self._current_cam_frame_interval, validation_only)

        return res

    def get_current_parameters(self) -> AcquisitionParameters:
        """
        Return current parameters of the camera

        :return: Parameters object
        """
        return AcquisitionParameters(led_delay_us=self._trigger.get_flash_delay(self),
                                     led_duration_us=self._trigger.get_flash_duration(self),
                                     double_strobe_delay_us=self._trigger.get_double_strobe_delay(self),
                                     drops_per_img=self._trigger.get_drops_per_img(self),
                                     jetting_frequency=self._trigger.get_jetting_frequency())

    def get_valid_parameters(self) -> Tuple[AcquisitionParameters, AcquisitionParameters]:
        """
        Return the valid range of parameters (min,max)
        :return:
        """
        delay = self._trigger.get_flash_delay_range(self)
        duration = self._trigger.get_flash_duration_range(self)
        double_strobe_delay = self._trigger.get_double_strobe_delay_range(self)
        drops_per_img = self._trigger.get_drops_per_img_range(self)
        max_led_duration = self._led.max_led_on_time_us

        if duration[1] is not None:
            max_led_duration = min(duration[1], max_led_duration)

        # todo add frequency?
        minimum = AcquisitionParameters(led_delay_us=delay[0],
                                        led_duration_us=duration[0],
                                        double_strobe_delay_us=double_strobe_delay[0],
                                        drops_per_img=drops_per_img[0],
                                        jetting_frequency=None)

        maximum = AcquisitionParameters(led_delay_us=delay[1],
                                        led_duration_us=max_led_duration,
                                        double_strobe_delay_us=double_strobe_delay[1],
                                        drops_per_img=drops_per_img[1],
                                        jetting_frequency=None)

        return minimum, maximum

    @abstractmethod
    def get_available_resolutions(self):
        """
        Return the available resolutions and their respective max. frequency

        :return: List of (resolution_x, resolution_y, max_fps, max_fps_string)
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _set_exposure_time(self, exposure_us, validation_only=False):
        """
        Set exposure time in camera if its not only managed via the trigger (Trigger signal will anyway stays high for
        the given exposure time.

        :param exposure_us:
        :return:
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_image_size_mm(self):
        """
        Return current image size in mm
        :return: (x, y) in mm
        """
        raise NotImplementedError("Please Implement this method")

    def um_per_px(self) -> [float, float]:
        return self._resolution_um_per_px

    @abstractmethod
    def get_max_fps(self):
        """
        Returns the maximum camera framerate for the current config (depends on resolution, ROI, pixel depth, ...)
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def set_roi(self, roi):
        """
        FEATURE: ROI concept not implemented yet

        :param roi:
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_image(self, pull=False):
        """
        Get the last image received.

        :param pull: if True, will consume the image buffer (= next read will returns None until the next frame arrives)
        :return: None if no image
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def list_available_camera(cls) -> list:
        """
        If possible, list all cameras found. This should be available even without any instance of the camera class
        :return:
        """
        return [cls.__name__ + ': Cannot list cameras']
