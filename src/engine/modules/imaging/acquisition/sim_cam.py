import logging
import os
import threading
import time
from typing import Optional
import cv2

from engine.helpers.decorators import singleton
from engine.modules.imaging.acquisition.camera import Camera
from queue import Empty

from engine.modules.imaging.acquisition.sim_trigger import SimTrigger

logger = logging.getLogger(__name__)


# Make it a singleton so we can link both sim-camera and sim-trigger class without too many work
@singleton
class SimCam(Camera):
    """
    Simulation with bitmaps, works together with SimTrigger
    """
    CONFIG_PATH = 'sim'

    def __init__(self):
        super().__init__()
        self._config.add_required_key(['images_path', 'resolution_x', 'resolution_y', 'fps_max'])
        self._is_open = False

        self._appsink = None
        self._source = None
        self._webrtc = None

        self._stop_request = threading.Event()
        self._worker = None
        self._thread_period = None

        self._frame_count = 0

    def worker_thread(self):
        """
        Separate thread emulating camera HW, read an image for each trigger.
        :return:
        """
        frame_counter = 0

        while not self._stop_request.is_set():
            start = time.time()

            # read trigger if any
            if not SimTrigger.cam_trigger_eventqueue.empty():
                try:
                    trig_event: Optional[SimTrigger.SimCamTriggerEvent] = SimTrigger.cam_trigger_eventqueue.get_nowait()
                except Empty:
                    trig_event = None

                # generate an image
                if trig_event:
                    frame_counter += 1
                    # todo take correct image depending on trigger value
                    # trig_event.led_delay, etc...
                    image_path = self._config.get_property('images_path')
                    for file in os.listdir(image_path):
                        if file.endswith(".bmp") or file.endswith(".png"):
                            filename = os.path.join(image_path, file)

                            img = cv2.imread(filename, 1)
                            r, g, b = cv2.split(img)
                            # TODO send image somewhere!
                            logger.debug(f'New SimCam frame {frame_counter}')
                            break

            elapsed = time.time() - start

            if elapsed > self._thread_period:
                logger.warning(f'Thread period too slow for SimCam (or max FPS too high!),'
                               f'current: {elapsed}, target: {self._thread_period}')

            time.sleep(max(self._thread_period - elapsed, 0))

    def _configure(self):
        resolution = [self._config.get_property('resolution_x'), self._config.get_property('resolution_y')]
        framerate = self._config.get_property('cam_framerate')

        # TODO emulate camera

        return True

    def open(self):
        """
        Open HW connection

        :return: True if succeeded, False otherwise
        """
        self._thread_period = 0.8 / self._config.get_property(['fps_max'])  # thread period = twice the maximum FPS

        if self._start_pipeline():
            self._worker = threading.Thread(target=self.worker_thread, name='SimCam_worker', daemon=True)
            self._worker.start()
            self._is_open = True

        return self._is_open

    def close(self):
        """
        Close HW connection

        :return: True if succeeded, False otherwise
        """
        self._is_open = False
        if self._worker is not None and self._worker.is_alive():
            self._stop_request.set()  # kill worker thread
            self._worker.join()
            self._stop_request.clear()

        return self._is_open

    def enable_freewheel(self, fps=None):
        return True

    def disable_freewheel(self):
        return True

    def get_available_resolutions(self):
        return [self._config.get_property('resolution_x'), self._config.get_property('resolution_y'), f"{self._config.get_property(['fps_max'])}/1", self._config.get_property(['fps_max'])]

    def get_image(self, pull=False):
        """
        Get the latest image from the camera using its internal appsink

        :return:
        """
        raise NotImplementedError

    def get_max_fps(self):
        return self._config.get_property(['fps_max'])

    def set_roi(self, roi):
        raise NotImplementedError

    def _set_exposure_time(self, exposure_us, validation_only=False):
        return True

    def get_image_size_mm(self):
        resolution = [self._config.get_property('resolution_x'), self._config.get_property('resolution_y')]
        mm_per_px = self._config.get_property('um_per_px')/1000.0
        return [r * mm_per_px for r in resolution]

    @classmethod
    def list_available_camera(cls) -> list:
        return []
