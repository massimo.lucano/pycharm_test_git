import logging
from abc import abstractmethod, ABC

import datetime
import numpy as np

from engine.common.config_manager import ConfigManager

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from engine.modules.imaging.acquisition.camera import Camera

logger = logging.getLogger(__name__)


class Trigger(ABC):
    """
    Base class to interface a trigger board.

    Trigger has two main outputs: one to trigger the print system and make the printhead jetting, the other to trigger the camera and acquire frames.

    We can therefore manage both features in an independant way: start/stop jetting, and start/stop acquiring.
    """
    CONFIG_PATH = 'triggers'

    def __init__(self, ):
        super().__init__()
        self._config = ConfigManager(self.CONFIG_PATH)
        self._config.add_required_key(['name', 'enable', 'frequency_master', 'support_parallel_trigger', 'support_burst_trigger'])
        self._virtual_frequency = None  # if trigger is not the frequency master, keeps track of the current frequency (i.e to check for safe conditions of the LED)

        self._last_jetting_start_time = np.datetime64(0, 's')
        self._last_jetting_stop_time = np.datetime64(0, 's')

    def set_config(self, config_manager, root_key):
        self._virtual_frequency = None  # invalidate saved values
        return self._config.set_config_from_parent(config_manager, root_key)

    def is_frequency_master(self):
        """
        Check if Trigger is generating the frequency for the print system, or if the print system is the master for the
        frequency

        :return: True if trigger is the master for the frequency, False otherwise
        """
        return self._config.get_property(['frequency_master'])

    def is_burst_capable(self):
        """
        Check if Trigger is able to generate drop burst and stop after a given amount of drops
        frequency

        :return: True if trigger is capable, false otherwise
        """
        return self._config.get_property(['support_burst_trigger'])

    @property
    def name(self):
        if self._config:
            return self._config.get_property(['name'])
        else:
            return None

    @abstractmethod
    def open(self) -> bool:
        """
        Open HW connection

        :return: True if succeeded, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    @property
    def support_parallel_trigger(self):
        # FEATURE do not take it from the config, but from the implementation (HW dependant)
        return self._config.get_property('support_parallel_trigger')

    @property
    def last_jetting_start_time(self):
        return self._last_jetting_start_time

    @property
    def last_jetting_stop_time(self):
        return self._last_jetting_stop_time

    def get_jetting_frequency(self):
        if self.is_frequency_master():
            return self._get_jetting_frequency()
        else:
            return self._virtual_frequency

    @abstractmethod
    def _get_jetting_frequency(self):
        raise NotImplementedError("Please Implement this method")

    def set_jetting_frequency(self, frequency, validation_only=False):
        """
        Set the target jetting frequency.
        Even if the trigger is not the frequency master, the target frequency is helping to check whether the cameras
        and leds are in their normal operating range

        :param frequency:
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if self.is_frequency_master():
            return self._set_jetting_frequency(frequency, validation_only)
        else:
            if not validation_only:
                self._virtual_frequency = frequency
            return True

    @abstractmethod
    def _set_jetting_frequency(self, frequency, validation_only=False):
        """
        Set the target jetting frequency

        :param frequency:
        :param validation_only: if True, check only if the value is valid or not
        :return: True if valid and applied, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    def set_camera_divider(self, camera: 'Camera', divider, validation_only=False):
        """
        Set how often we take a picture of a drop

        :param divider:
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if self._check_led_safe_conditions(camera, camera_divider=divider):
            return self._set_camera_divider(camera, divider, validation_only)
        else:
            return False

    @abstractmethod
    def _set_camera_divider(self, camera: 'Camera', divider, validation_only=False):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_camera_divider(self, camera: 'Camera'):
        raise NotImplementedError("Please implement this method")

    @abstractmethod
    def get_flash_delay(self, camera: 'Camera'):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_flash_delay_range(self, camera: 'Camera'):
        raise NotImplementedError("Please Implement this method")

    def set_flash_delay(self, camera: 'Camera', delay_us, validation_only=False):
        """
        Set the delay for flashing

        :param delay_us: delay in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        freq = self.get_jetting_frequency()
        if freq is not None:
            # check if the delay is not longer than X times the jetting period
            current_period = 1000000 / freq
            if delay_us > 2 * current_period:
                logger.warning(
                    f"Flash delay is longer than 2x the jetting period. Please check that you are looking at the right drop")
                # return False  # TODO check if we need that, and how to handle it if yes.
        else:
            logger.warning("Failed to get frequency information")

        return self._set_flash_delay(camera, delay_us, validation_only)

    @abstractmethod
    def _set_camera_delay(self, camera: 'Camera', delay_us, validation_only=False):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_camera_delay(self, camera: 'Camera'):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def set_camera_duration(self, camera: 'Camera', duration_us, validation_only=False):
        """
        Set the duration for camera

        :param duration_us: duration in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_camera_duration(self, camera: 'Camera'):
        raise NotImplementedError("Please Implement this method")

    def set_camera_delay(self, camera: 'Camera', delay_us, validation_only=False):
        """
        Set the delay for camera shutter

        :param delay_us: delay in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        # todo check if not overtriggered
        return self._set_camera_delay(camera, delay_us, validation_only)

    @abstractmethod
    def _set_flash_delay(self, camera: 'Camera', delay_us, validation_only=False):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_flash_duration(self, camera: 'Camera'):
        raise NotImplementedError

    @abstractmethod
    def get_flash_duration_range(self, camera: 'Camera'):
        raise NotImplementedError

    def set_flash_duration(self, camera: 'Camera', duration_us, validation_only=False):
        """
        Set the duration for flashing

        :param camera: 'Camera' object linked to the request
        :param duration_us: duration in microseconds
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if self._check_led_safe_conditions(camera, duration_us=duration_us):
            return self._set_flash_duration(camera, duration_us, validation_only)
        else:
            return False

    @abstractmethod
    def _set_flash_duration(self, camera: 'Camera', duration_us, validation_only=False):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_drops_per_img(self, camera: 'Camera'):
        raise NotImplementedError

    @abstractmethod
    def get_drops_per_img_range(self, camera: 'Camera'):
        raise NotImplementedError

    def set_drops_per_img(self, camera: 'Camera', drops_per_img, validation_only=False):
        """
        Set the number of drops to flash on a single image

        :param drops_per_img: number of drops per image
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if self._check_led_safe_conditions(camera, drops_per_img=drops_per_img):
            return self._set_drops_per_img(camera, drops_per_img, validation_only)
        else:
            return False

    @abstractmethod
    def _set_drops_per_img(self, camera: 'Camera', drops_per_img, validation_only=False):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def get_double_strobe_delay(self, camera: 'Camera'):
        raise NotImplementedError

    @abstractmethod
    def get_double_strobe_delay_range(self, camera: 'Camera'):
        raise NotImplementedError

    @abstractmethod
    def set_double_strobe_delay(self, camera: 'Camera', double_strobe_delay_us=None, validation_only=False):
        """
        Set the delay between the two strobes or deactivate it if None.

        :param double_strobe_delay_us: delay between the strobe or None to deactivate this feature
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        # TODO check for led safe condition (look at the other parameters)
        raise NotImplementedError("Please Implement this method")

    def start_acquisition(self, cam_frames=None, camera=None, print_enable=True):
        """
        Start triggering camera for X frames (wait if not jetting)

        :param cam_frames:
        :param print_enable: True if the print system should also receive the trigger pulses
        :return:
        """
        self._last_jetting_start_time = np.datetime64(datetime.datetime.now())  # TODO add some delay for the writing?

        if cam_frames is not None and self.is_burst_capable():
            # if the trigger automatically stops after a given amount of drops, estimate the stop time
            self._last_jetting_stop_time = self._last_jetting_start_time + np.timedelta64(cam_frames/self._get_jetting_frequency(), 's')

        return self._start_acquisition(cam_frames, camera, print_enable)

    def stop_acquisition(self):
        self._last_jetting_stop_time = np.datetime64(datetime.datetime.now())
        return self._stop_acquisition()

    def stop_jetting(self):
        if self.is_acquiring():
            self._stop_acquisition()
        self._stop_jetting()

    @abstractmethod
    def _start_acquisition(self, cam_frames=None, camera=None, print_enable=True):
        """
        Start trigger for X frames
        :param cam_frames:
        :param print_enable: True if the print system should also receive the trigger pulses
        :return:
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _stop_acquisition(self):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def is_acquiring(self):
        raise NotImplementedError("Please implement")

    @abstractmethod
    def start_jetting(self):  # TODO: remove, use _start_acquisition(print_enable) parameter instead!
        """ Start sending pulses to the print system """
        raise NotImplementedError("Please implement")

    @abstractmethod
    def _stop_jetting(self):
        """ Stop sending pulses to the print system """
        raise NotImplementedError("Please implement")

    @abstractmethod
    def is_jetting(self):
        raise NotImplementedError("Please implement")

    @abstractmethod
    def close(self):
        """
        Close HW connection

        :return: True if succeeded, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    def _check_led_safe_conditions(self, camera: 'Camera', frequency=None, duration_us=None, drops_per_img=None, camera_divider=None):
        """
        Check if LED parameters are safe. If some arguments are missing (None), read them first.
        :param self:
        :param camera:
        :param frequency:
        :param duration_us:
        :param drops_per_img:
        :param camera_divider:
        :return: True if the LED is in safe operating range, False if not or if we are not able to check it properly
        """
        return True  # todo delegate this check to HW
