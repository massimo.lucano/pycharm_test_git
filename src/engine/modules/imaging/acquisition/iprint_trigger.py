import logging
import threading

import serial
from enum import Enum, IntEnum

from engine.modules.imaging.acquisition.trigger import Trigger

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from engine.modules.imaging.acquisition.camera import Camera

logger = logging.getLogger(__name__)


class RegisterDescriptor(object):
    class RegisterType(Enum):
        READ_ONLY = 0
        READ_WRITE = 1
        WRITE_ONLY = 2

    """
    A descriptor that models reading and writing to hardware registers.

    inspired from https://stackoverflow.com/questions/25866733/python-hardware-handling-how-to-efficiently-implement-hardware-registers-as-a
    """

    def __init__(self, address, reg_type: RegisterType = RegisterType.READ_WRITE, size=4, read_cached=True):
        """
        Create a new register description

        :param address:
        :param size: in bytes
        """
        self.address = address
        self.size = size
        self.reg_type: RegisterDescriptor.RegisterType = reg_type
        self._read_cache_enable = read_cached
        self._cache_value = None
        # TODO add allowed range (to check input from user)

    def __get__(self, obj, type=None):
        if not isinstance(obj, IprintTrigger):
            return self
        if self.reg_type == self.RegisterType.WRITE_ONLY:
            raise AttributeError("Write only attribute")

        if self._read_cache_enable:
            if self._cache_value is None:
                self._cache_value = obj._register_read(self.address)
            return self._cache_value
        else:
            return obj._register_read(self.address)

    def __set__(self, obj, value):
        if not isinstance(obj, IprintTrigger):
            raise AttributeError("Only IprintTrigger instance can set attribute")
        if self.reg_type == self.RegisterType.READ_ONLY:
            raise AttributeError("Read only attribute")
        if not obj._register_write(self.address,
                                   value):  # TODO check if we need the 'check_Write' argument (could be set globally in iprinttrigger class instead)
            raise AttributeError("Timeout while setting attribute")
        if self._read_cache_enable:
            self._cache_value = None  # invalidate cache after each write (we don't want to cache the write if the
            # read value doesn't match then


class RegisterDescriptorList(list):
    def __init__(self, initlist, instance):
        self.instance = instance
        super(RegisterDescriptorList, self).__init__(initlist)

    def __getitem__(self, index):
        return super(RegisterDescriptorList, self).__getitem__(index).__get__(self.instance, self.instance.__class__)

    def __setitem__(self, index, value):
        super(RegisterDescriptorList, self).__getitem__(index).__set__(self.instance, value)


class IprintTrigger(Trigger):
    """
    Interface to iPrint Dropwatching FPGA based on Mimas board.

    We assume that one LED is connected to LED0 and one Grayscale camera to CAM0.
    Register Led0 and Led1 are OR'ed to the ouptut LED0 for double strobe purpose.
    LED is flashing all drops and AND'ed by Camera trigger.
    In case of overlaid imaging (drops per img > 1), camera trigger is kept open over several drops.
    """

    CONFIG_PATH = 'iprint-fpga'

    INTERNAL_CLOCK_PERIOD_NS = 10

    class ClockSource(IntEnum):
        QEI_INPUT_0 = 0
        QEI_INPUT_1 = 1
        EXTERNAL_TRIGGER_RISING_EDGE = 9
        EXTERNAL_TRIGGER_FALLING_EDGE = 10
        INTERNAL = 16

    class InternalClockSettings(IntEnum):
        ENABLE_BIT = 1
        DROP_LIMITATION_ENABLE_BIT = 2
        DROP_LIMITATION_START_BIT = 4

    class QeiOutputSettings(IntEnum):
        ENABLE_BIT = 1
        EMULATE_IDX_BIT = 4

    class CameraTriggerSettings(IntEnum):
        ENABLE_BIT = 1
        ENABLE_SLAVE_CAMERA_BIT = 2
        SINGLE_SHOT_ENABLE_BIT = 4
        SINGLE_SHOT_ARM_BIT = 8
        SLAVE_SOURCE_0_BITS = 0
        SLAVE_SOURCE_1_BITS = 256

    class LedTriggerSettings(IntEnum):
        ENABLE_BIT = 1

    # Reg description
    _reg_clock_source = RegisterDescriptor(0x01)

    _reg_version = RegisterDescriptor(0x00, RegisterDescriptor.RegisterType.READ_ONLY)
    _reg_reset = RegisterDescriptor(0x00, RegisterDescriptor.RegisterType.WRITE_ONLY)

    _reg_int_clock_settings = RegisterDescriptor(0x10)
    _reg_int_clock_freq_divider = RegisterDescriptor(0x11)
    _reg_int_clock_drop_limitation = RegisterDescriptor(0x13)

    _reg_qei_out_settings = RegisterDescriptor(0x20)
    _reg_qei_out_source_select = RegisterDescriptor(0x21)
    _reg_qei_out_divider = RegisterDescriptor(0x22)
    _reg_qei_out_idx_count = RegisterDescriptor(0x23)

    _reg_qei_in0_settings = RegisterDescriptor(0x40)
    _reg_qei_in0_edges_per_drop = RegisterDescriptor(0x41)
    _reg_qei_in0_position = RegisterDescriptor(0x42)
    _reg_qei_in0_edge_mask = RegisterDescriptor(0x43)

    _reg_cam_settings = [RegisterDescriptor(addr) for addr in range(0x80, 0xB0 + 1, 0x10)]
    _reg_cam_delay = [RegisterDescriptor(addr) for addr in range(0x81, 0xB1 + 1, 0x10)]
    _reg_cam_duration = [RegisterDescriptor(addr) for addr in range(0x82, 0xB2 + 1, 0x10)]
    _reg_cam_drops_per_trigger = [RegisterDescriptor(addr) for addr in range(0x83, 0xB3 + 1, 0x10)]

    _reg_led_settings = [RegisterDescriptor(addr) for addr in range(0xC0, 0xF0 + 1, 0x10)]
    _reg_led_delay = [RegisterDescriptor(addr) for addr in range(0xC1, 0xF1 + 1, 0x10)]
    _reg_led_duration = [RegisterDescriptor(addr) for addr in range(0xC2, 0xF2 + 1, 0x10)]
    _reg_led_drops_per_trigger = [RegisterDescriptor(addr) for addr in range(0xC3, 0xF3 + 1, 0x10)]
    _reg_led_master_cam = [RegisterDescriptor(addr) for addr in range(0xC4, 0xF4 + 1, 0x10)]

    def __init__(self):
        super().__init__()
        self._config.add_required_key(['port', 'baudrate', 'version'])
        self._serial = serial.Serial()
        self._RETRIES = 5

        self._serial_lock = threading.RLock()

        self._drops_per_img = [1]

        # See: https://stackoverflow.com/questions/13447017/python-a-list-of-descriptors
        self._reg_cam_settings = RegisterDescriptorList(self.__class__._reg_cam_settings, self)
        self._reg_cam_delay = RegisterDescriptorList(self.__class__._reg_cam_delay, self)
        self._reg_cam_duration = RegisterDescriptorList(self.__class__._reg_cam_duration, self)
        self._reg_cam_drops_per_trigger = RegisterDescriptorList(self.__class__._reg_cam_drops_per_trigger, self)
        self._reg_led_settings = RegisterDescriptorList(self.__class__._reg_led_settings, self)
        self._reg_led_delay = RegisterDescriptorList(self.__class__._reg_led_delay, self)
        self._reg_led_duration = RegisterDescriptorList(self.__class__._reg_led_duration, self)
        self._reg_led_drops_per_trigger = RegisterDescriptorList(self.__class__._reg_led_drops_per_trigger, self)
        self._reg_led_master_cam = RegisterDescriptorList(self.__class__._reg_led_master_cam, self)

    def open(self):
        """
        Open serial port and check the version of the FPGA

        :return: True is port is successfully open, False otherwise
        """
        with self._serial_lock:
            self._serial.baudrate = self._config.get_property(["baudrate"])
            self._serial.port = self._config.get_property(["port"])
            self._serial.timeout = 1
            self._serial.write_timeout = 1
            self._serial.parity = serial.PARITY_ODD
            try:
                self._serial.open()
            except serial.SerialException as e:
                logger.error("Error while connecting to iPrint Trigger:" + str(e))
                return False

            if self._serial.is_open:
                # check version
                if self._register_read(0x00) == self._config.get_property(["version"]):
                    return self._init_registers()
                else:
                    logger.error('Wrong FPGA version detected')
                    return False
            else:
                logger.error('Cannot open FPGA serial')
                return False

    def _init_registers(self):
        """
        Set default values for all registers
        :return:
        """
        try:
            if self.is_frequency_master():
                self.init_FPGA_dropping_mode()
            else:
                self.init_FPGA_printing_mode()

        except AttributeError:
            logger.error("Settings default configuration failed")
            return False

        return True

    def _get_jetting_frequency(self):
        try:
            period_ns = self.INTERNAL_CLOCK_PERIOD_NS * self._reg_int_clock_freq_divider
            if period_ns > 0:
                return 1000 * 1000 * 1000 / period_ns
            else:
                return None
        except AttributeError:
            logger.error("Failed to read back frequency from IprintTrigger")
            return None

    def _set_jetting_frequency(self, frequency, validation_only=False):
        """
        Set the target jetting frequency

        :param frequency:
        :param validation_only: if True, check only if the value is valid or not
        :return: True if valid and applied, False otherwise
        """
        if frequency <= 0:
            return False

        period = int((1000 * 1000 * 1000 / self.INTERNAL_CLOCK_PERIOD_NS) // frequency)

        if period > 0:
            # update frequency
            try:
                if not validation_only:
                    self._reg_int_clock_freq_divider = period
            except AttributeError:
                logger.error(f"Error while updating frequency divider to {period}")
                return False

            return True
        else:
            return False

    def _set_camera_divider(self, camera: 'Camera', divider, validation_only=False):
        """
        Set how often we take a picture of a drop

        :param divider:
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        divider = int(divider)

        if divider <= 0:
            return False

        # todo check trigger_idx everywhere (check based on config file?)
        # todo change register according to trigger_idx
        # camera.trigger_idx
        try:
            if not validation_only:
                self._reg_cam_drops_per_trigger[camera.trigger_idx] = divider
        except AttributeError:
            logger.error(f"Error while setting camera divider to {divider}")
            return False
        return True

    def get_camera_divider(self, camera: 'Camera'):
        try:
            # todo manage trigger_idx range
            return self._reg_cam_drops_per_trigger[camera.trigger_idx]
        except AttributeError:
            logger.error("Failed to get camera divider")
            return None

    def get_flash_delay(self, camera: 'Camera'):
        try:
            # todo manage trigger_idx
            idx = camera.led.trigger_idx
            if idx is None:
                return None

            return (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_led_delay[idx]) / 1000
        except AttributeError:
            logger.error("Failed to get flash delay")
            return None

    def get_flash_delay_range(self, camera: 'Camera'):
        return self.INTERNAL_CLOCK_PERIOD_NS/1000.0, 1000.0  # todo TBD

    def _set_flash_delay(self, camera: 'Camera', delay_us, validation_only=False):
        """
        Set the delay for flashing

        :param delay_us: delay in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        try:
            value = int(delay_us * 1000 // self.INTERNAL_CLOCK_PERIOD_NS)

            if value <= 0:
                logger.error(f"Failed to set flash delay, too short {delay_us}us")
                return False

            if not validation_only:
                # camera.led.trigger_idx
                self._reg_led_delay[camera.led.trigger_idx] = value
        except AttributeError:
            logger.error("Failed to set flash delay")
            return False

        return True

    def get_camera_delay(self, camera: 'Camera'):
        try:
            idx = camera.trigger_idx
            if idx is None:
                return None

            return (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_cam_delay[idx]) / 1000
        except AttributeError:
            logger.error("Failed to get camera delay")
            return None

    def _set_camera_delay(self, camera: 'Camera', delay_us, validation_only=False):
        """
        Set the delay for camera

        :param delay_us: delay in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        try:
            value = int(delay_us * 1000 // self.INTERNAL_CLOCK_PERIOD_NS)

            if value <= 0:
                logger.error(f"Failed to set camera delay, too short {delay_us}us")
                return False

            if not validation_only:
                # camera.trigger_idx
                self._reg_cam_delay[camera.trigger_idx] = value
        except AttributeError:
            logger.error("Failed to set camera delay")
            return False

        return True

    def get_camera_duration(self, camera: 'Camera'):
        try:
            idx = camera.trigger_idx
            if idx is None:
                return None

            return (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_cam_duration[idx]) / 1000
        except AttributeError:
            logger.error("Failed to get camera duration")
            return None

    def set_camera_duration(self, camera: 'Camera', duration_us, validation_only=False):
        """
        Set the duration for camera

        :param duration_us: duration in microseconds
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        try:
            value = int(duration_us * 1000 // self.INTERNAL_CLOCK_PERIOD_NS)

            if value <= 0:
                logger.error(f"Failed to set camera duration, too short {duration_us}us")
                return False

            if not validation_only:
                # camera.trigger_idx
                self._reg_cam_duration[camera.trigger_idx] = value
        except AttributeError:
            logger.error("Failed to set camera delay")
            return False
        return True

    def get_flash_duration(self, camera: 'Camera'):
        try:
            # todo manage trigger_idx range
            return (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_led_duration[camera.led.trigger_idx]) / 1000
        except AttributeError:
            logger.error("Failed to get flash duration")
            return None

    def get_flash_duration_range(self, camera: 'Camera'):
        return self.INTERNAL_CLOCK_PERIOD_NS/1000.0, 3.0  # TODO max duration TBD

    def _set_flash_duration(self, camera: 'Camera', duration_us, validation_only=False):
        """
        Set the duration for flashing

        :param camera: 'Camera' object linked to the request
        :param duration_us: duration in microseconds
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        try:
            value = int(duration_us * 1000 // self.INTERNAL_CLOCK_PERIOD_NS)

            if value <= 0:
                logger.error(f"Failed to set flash duration, too short {duration_us}us")
                return False

            if not validation_only:
                self._reg_led_duration[camera.led.trigger_idx] = value
                self._reg_led_duration[camera.led.trigger_idx_double_strobe] = value
        except AttributeError:
            logger.error("Failed to set flash duration")
            return False

        return True

    def get_drops_per_img(self, camera: 'Camera'):
        return self._drops_per_img[camera.trigger_idx]

    def _set_drops_per_img(self, camera: 'Camera', drops_per_img, validation_only=False):
        if not validation_only:
            self._drops_per_img[camera.trigger_idx] = drops_per_img
        return True

    def get_drops_per_img_range(self, camera: 'Camera'):
        return 1, 1000  # todo TBD - depends on frequency and max shutter open time

    def get_double_strobe_delay(self, camera: 'Camera'):
        try:
            if not self._reg_led_settings[camera.led.trigger_idx_double_strobe] & IprintTrigger.LedTriggerSettings.ENABLE_BIT:
                return 0  # Second LED not enabled -> double strobe delay = 0

            # todo manage trigger_idx range
            led_delay1 = (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_led_delay[camera.led.trigger_idx]) / 1000
            led_delay2 = (self.INTERNAL_CLOCK_PERIOD_NS * self._reg_led_delay[camera.led.trigger_idx_double_strobe]) / 1000
            double_strobe_delay = led_delay2 - led_delay1
            if double_strobe_delay <= 0:
                logger.error("Invalid strobe delay from FPGA!")
                return None
            else:
                return double_strobe_delay
        except AttributeError:
            logger.error("Failed to get double strobe delay")
            return None

    def get_double_strobe_delay_range(self, camera: 'Camera'):
        return 0, 1000.0

    def set_double_strobe_delay(self, camera: 'Camera', double_strobe_delay_us=None, validation_only=False):
        """
        Set the delay between the two strobes or deactivate it if None.

        :param double_strobe_delay_us: delay between the strobe or None to deactivate this feature
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        try:
            if double_strobe_delay_us <= 0:
                # deactivate double strobe
                if not validation_only:
                    self._reg_led_settings[camera.led.trigger_idx_double_strobe] &= (~IprintTrigger.LedTriggerSettings.ENABLE_BIT)
            else:
                first_delay = self.get_flash_delay(camera)

                if first_delay is None:
                    logger.error("Failed to get first flash delay while setting double strobe delay")
                    return False

                value = int((first_delay + double_strobe_delay_us) * 1000 // self.INTERNAL_CLOCK_PERIOD_NS)

                if value <= 0:
                    logger.error(f"Failed to set double strobe delay, too short {double_strobe_delay_us}us")
                    return False

                if not validation_only:
                    self._reg_led_delay[camera.led.trigger_idx_double_strobe] = value

                    # enable double strobe
                    self._reg_led_settings[
                        camera.led.trigger_idx_double_strobe] |= IprintTrigger.LedTriggerSettings.ENABLE_BIT
        except AttributeError:
            logger.error("Failed to set double strobe delay")
            return False
        return True

    def _start_acquisition(self, cam_frames=None, camera=None, print_enable=True):
        # self._reg_int_clock_settings &= ~IprintTrigger.InternalClockSettings.ENABLE_BIT  # stop main clock to prepare all outputs
        if print_enable:
            self.start_jetting()
        else:
            self.stop_jetting()
        self._reg_cam_settings[0] |= IprintTrigger.CameraTriggerSettings.ENABLE_BIT
        # self._reg_int_clock_settings |= IprintTrigger.InternalClockSettings.ENABLE_BIT
        return True

    def _stop_acquisition(self):
        """
        Stop acquisition but keep the print trigger running for the print system
        :return:
        """
        self._reg_cam_settings[0] &= ~IprintTrigger.CameraTriggerSettings.ENABLE_BIT
        return False

    def is_acquiring(self):
        return self._reg_cam_settings[0] & IprintTrigger.CameraTriggerSettings.ENABLE_BIT > 0

    def start_jetting(self):
        """
        Start printing trigger
        :return:
        """
        if not self.is_frequency_master():
            logger.error("Cannot start printing trigger if not frequency master")
            return False
        self._reg_qei_out_settings |= IprintTrigger.QeiOutputSettings.ENABLE_BIT
        return True

    def _stop_jetting(self):
        if not self.is_frequency_master():
            logger.error("Cannot stop printing trigger if not frequency master")
            return False
        self._reg_qei_out_settings &= ~IprintTrigger.QeiOutputSettings.ENABLE_BIT
        return True

    def is_jetting(self):
        if not self.is_frequency_master():
            logger.error("Cannot check if printing if not frequency master")
            return None
        return self._reg_qei_out_settings & IprintTrigger.QeiOutputSettings.ENABLE_BIT > 0

    def close(self):
        """
        Close the previously opened serial port

        :return: True is port is successfully closed
        """
        self._stop_jetting()

        with self._serial_lock:
            self._serial.close()
            if not self._serial.is_open:
                return True
            else:
                logger.error('Cannot close FPGA serial')
                return False

    def _register_read(self, register: int):
        """
        Read the specified register

        :param register:
        :return:
        """
        trials = self._RETRIES

        register = register + 8
        data = 0
        msg = register.to_bytes(1, byteorder='big') + data.to_bytes(4, byteorder='big')

        while trials > 0:
            trials -= 1

            with self._serial_lock:
                self._serial.reset_input_buffer()

                try:
                    # send cmd
                    byte_written = self._serial.write(msg)

                    # read response
                    bytes_read = self._serial.read(5)  # read_until(expected=LF, size=None)
                except serial.serialutil.SerialTimeoutException:
                    logger.error("SerialTimeoutExecption while reading FPGA register, reset com...")
                    self._reset_com()
                    # TODO also close/open? -> but without reinit!
                    continue


                if len(bytes_read) == 5:
                    response = int.from_bytes(bytes_read[1:], byteorder='big')
                    return response
                else:
                    # no response, reset fpga com buffer and retry
                    logger.error(f"FPGA communication failed to read register {register}")
                    self._reset_com()

    def _register_write(self, register: int, data: int, check_write=False):
        """
        Write the specified data in a register.

        :param register:
        :param data:
        :param check_write: perform a register read to check that value is well-written
        :return:
        """
        trials = self._RETRIES

        try:
            msg = register.to_bytes(1, byteorder='big') + data.to_bytes(4, byteorder='big')
        except OverflowError:
            raise AttributeError("Int is too big to fit on 4 bytes")
        while trials > 0:
            trials -= 1

            with self._serial_lock:
                self._serial.reset_input_buffer()

                # send cmd
                try:
                    byte_written = self._serial.write(msg)
                except serial.serialutil.SerialTimeoutException:
                    logger.error("SerialTimeoutExecption while writting FPGA register, reset com...")
                    self._reset_com()
                    # TODO also close/open? -> but without reinit!
                    continue

                if check_write:
                    if self._register_read(register) == data:
                        return True
                    else:
                        logger.warning('Fpga write failed, retry...')
                else:
                    return True

        return False

    def _reset_com(self):
        logger.warning('Reset FPGA com')
        register = 0
        data = 0xffffffff
        msg = register.to_bytes(1, byteorder='big') + data.to_bytes(4, byteorder='big')
        with self._serial_lock:
            byte_written = self._serial.write(msg)

    def dump_register_state(self):
        """
        Debug purpose
        :return:
        """
        for i in range(2):
            print(f"LED {i}")
            print("--------")
            print(f'settings\t{self._reg_led_settings[i]:4x}')
            print(f'delay\t{self._reg_led_delay[i]:4x}')
            print(f'duration\t{self._reg_led_duration[i]:4x}')
            print(f'master cam\t{self._reg_led_master_cam[i]:4x}')
            print(f'drops per trig\t{self._reg_led_drops_per_trigger[i]:4x}')

    def init_FPGA_dropping_mode(self):
        """
        MPL FPGA init dropping mode
        :return:
        """
        self._reg_clock_source = IprintTrigger.ClockSource.INTERNAL
        self._reg_int_clock_settings = 0x0 | IprintTrigger.InternalClockSettings.ENABLE_BIT  # enable internal clock

        # self._reg_int_clock_freq_divider = int((1e9 / self.INTERNAL_CLOCK_PERIOD_NS) // 2)
        self._set_jetting_frequency(frequency=10)

        self._reg_qei_out_settings = 0x0 | IprintTrigger.QeiOutputSettings.ENABLE_BIT
        self._reg_qei_out_source_select = 0x04
        self._reg_qei_out_divider = 0x00

        # FEATURE define what is the default config when we are not the master for the frequency
        for i in range(4):
            self._reg_cam_settings[i] = 0x0 | IprintTrigger.CameraTriggerSettings.ENABLE_BIT
            self._reg_cam_delay[i] = 1000  # 10us
            self._reg_cam_duration[i] = 5000  # 50us
            self._reg_cam_drops_per_trigger[i] = 1

            self._reg_led_settings[i] = 0x0 | IprintTrigger.LedTriggerSettings.ENABLE_BIT if i == 0 else 0x0
            # enable only the first LED
            self._reg_led_delay[i] = 1800  # 18us
            self._reg_led_duration[i] = 100  # 500ns
            self._reg_led_master_cam[i] = 0
            self._reg_led_drops_per_trigger[i] = 1

        # Temp: trigger for Polytype print system on second CAM port:
        #self._reg_cam_settings[1] = 0x0 | IprintTrigger.CameraTriggerSettings.ENABLE_BIT
        #self._reg_cam_delay[1] = 0  # 0us
        #self._reg_cam_duration[1] = 500  # 5us
        #self._reg_cam_drops_per_trigger[1] = 1

    def init_FPGA_printing_mode(self):
        """
        MPL FPGA init printing mode
        :return:
        """
        QUE_inputChannel = 0

        self._reg_led_settings = 0x00 # register C0..F0 all leds disabled

        self._reg_cam_settings = 0x00 # register 80..B0 all camera triggers disabled

        self._reg_clock_source = QUE_inputChannel

        self._reg_qei_in0_settings = 0x01 #enable encoder 0 input

        self._reg_qei_out_settings = 0x01 #enable encoder 0 output

        self._reg_qei_out_source_select = 0x05 #Output encoder is linked to the internal selector (reg 01) which is linked to the encoder input
        self._reg_qei_out_divider = 0x01  #no divider at the output (internal frequency = print frequency)
    """
    MatLab iClass code
    % turn off the led trigger
    obj.FPGAComm.registerWrite(dec2hex((12+(obj.LEDOutputChannel))*16+0),0); %C..F0, ledEnable to false

    % turn off the cam trigger
    obj.FPGAComm.registerWrite(dec2hex((8+(obj.camTriggerOutputChannel))*16+0), 0); %8..B0, camera trigger disable

    % go to external encoder input
    obj.FPGAComm.registerWrite('01', obj.inputChannel); % select the encoder input we are using
    obj.FPGAComm.registerWrite(dec2hex((4+(obj.inputChannel))*16+0), 1); %4..70, encoder input enable
    obj.FPGAComm.registerWrite('20',1); % Output encoder enable
    obj.FPGAComm.registerWrite('21',5); % Output encoder is linked to the internal selecter (reg 01) which is linked to the encoder input
    obj.FPGAComm.registerWrite('22',1); % no divider at the output (internal frequency = print frequency)
    """
    """
    # Reg description
    _reg_clock_source = RegisterDescriptor(0x01)

    _reg_version = RegisterDescriptor(0x00, RegisterDescriptor.RegisterType.READ_ONLY)
    _reg_reset = RegisterDescriptor(0x00, RegisterDescriptor.RegisterType.WRITE_ONLY)

    _reg_int_clock_settings = RegisterDescriptor(0x10)
    _reg_int_clock_freq_divider = RegisterDescriptor(0x11)
    _reg_int_clock_drop_limitation = RegisterDescriptor(0x13)

    _reg_qei_out_settings = RegisterDescriptor(0x20)
    _reg_qei_out_source_select = RegisterDescriptor(0x21)
    _reg_qei_out_divider = RegisterDescriptor(0x22)
    _reg_qei_out_idx_count = RegisterDescriptor(0x23)

    _reg_qei_in0_settings = RegisterDescriptor(0x40)
    _reg_qei_in0_edges_per_drop = RegisterDescriptor(0x41)
    _reg_qei_in0_position = RegisterDescriptor(0x42)
    _reg_qei_in0_edge_mask = RegisterDescriptor(0x43)

    _reg_cam_settings = [RegisterDescriptor(addr) for addr in range(0x80, 0xB0 + 1, 0x10)]
    _reg_cam_delay = [RegisterDescriptor(addr) for addr in range(0x81, 0xB1 + 1, 0x10)]
    _reg_cam_duration = [RegisterDescriptor(addr) for addr in range(0x82, 0xB2 + 1, 0x10)]
    _reg_cam_drops_per_trigger = [RegisterDescriptor(addr) for addr in range(0x83, 0xB3 + 1, 0x10)]

    _reg_led_settings = [RegisterDescriptor(addr) for addr in range(0xC0, 0xF0 + 1, 0x10)]
    _reg_led_delay = [RegisterDescriptor(addr) for addr in range(0xC1, 0xF1 + 1, 0x10)]
    _reg_led_duration = [RegisterDescriptor(addr) for addr in range(0xC2, 0xF2 + 1, 0x10)]
    _reg_led_drops_per_trigger = [RegisterDescriptor(addr) for addr in range(0xC3, 0xF3 + 1, 0x10)]
    _reg_led_master_cam = [RegisterDescriptor(addr) for addr in range(0xC4, 0xF4 + 1, 0x10)]
    """