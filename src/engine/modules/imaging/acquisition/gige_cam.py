import logging
from collections import namedtuple

from engine.modules.imaging.acquisition.camera import Camera

logger = logging.getLogger(__name__)

DeviceInfo = namedtuple("DeviceInfo", "status name identifier connection_type")
CameraProperty = namedtuple("CameraProperty", "status value min max default step type flags category group")


class GigeCam(Camera):

    CONFIG_PATH = 'gige'

    def __init__(self):
        super().__init__()

        self._config.add_required_key(
            ['serial', 'gain', 'resolution_x', 'resolution_y', 'format',
             'cam_framerate'])  # camera specific only!

        self._available_resolutions = []

    def get_max_fps(self):
        return self._config.get_property(['fps_max'])  # FEATURE use HW value

    def _open_cam(self):
        # TODO open connection to HW

        try:
            self.set_property("Trigger Mode", "On")  # for GigE cameras
            self.set_property("Trigger Operation", "GlobalResetRelease")
            self.set_property("Trigger Source", "Any")  # Line1
            self.set_property("Trigger Activation", "RisingEdge")


            self.set_property("Exposure Auto", "Off")
            self.set_property("Exposure", int(self._config.get_property('shutter_min_duration_us')))

            self.set_property("Gain Auto", "Off")
            self.set_property("Gain", int(self._config.get_property('gain')))
        except:
            logger.error("Unable to open and configure camera")
            return False

        self._available_resolutions = self.__read_available_resolutions()

        return True

    def get_images_count(self):
        return self._frame_count

    def _close_cam(self):
        # TODO
        self._available_resolutions = []
        return True

    def _configure(self):
        """
        Update camera from the new config.

        :return:
        """
        # TODO
        return True

    def open(self):
        return self._open_cam()

    def close(self):
        return self._close_cam()

    def get_image_size_mm(self):
        """
        Return current image size in mm

        :return: (x, y) in mm
        """
        resolution = [self._config.get_property('resolution_x'), self._config.get_property('resolution_y')]
        return [a * b / 1000.0 for a, b in zip(resolution, self.um_per_px())]

    def get_image(self, pull=False):
        """
        Get the latest image from the camera using its internal appsink

        :param pull: remove image from buffer, return None if no image found
        :return:
        """
        raise NotImplementedError

    def enable_freewheel(self, fps=None):
        """
        Run camera with its internal trigger - for debug purpose

        :param fps:
        :return:
        """
        self.set_property("Trigger Mode", "Off")  # for GigE cameras

    def disable_freewheel(self):
        """
        Run camera with external trigger.

        :return:
        """
        self.set_property("Trigger Mode", "On")  # for GigE cameras

    def __read_available_resolutions(self):
        """
        Read all available resolution of the current camera with the configured video format only.
        :return:
        """
        # TODO
        return []

    def get_available_resolutions(self):
        """
        Get available resolutions and framerate for the current configured video format only

        :return:
        """
        return self._available_resolutions

    def _set_exposure_time(self, exposure_us, validation_only=False):
        if exposure_us < self._config.get_property('shutter_min_duration_us'):
            logger.warning(f"Cannot set camera exposure time < {self._config.get_property('shutter_min_duration_us')}.")
            return False
        try:
            if not validation_only:
                self.set_property("Exposure", int(exposure_us))
            return True
        except:
            logger.error("Unable to configure camera")
            return False

    def set_roi(self, roi):
        raise NotImplementedError

    # Camera properties
    def list_properties(self):
        """
        List all camera properties with their values

        :return:
        """
        raise NotImplementedError

    def get_property(self, PropertyName):
        """
        Get camera property

        :param PropertyName:
        :return:
        """
        raise NotImplementedError

    def set_property(self, PropertyName, value):
        """
        Set camera property

        :param PropertyName:
        :param value:
        :return:
        """
        # TODO
        return True

    @classmethod
    def list_available_camera(cls) -> list:
        raise NotImplementedError

