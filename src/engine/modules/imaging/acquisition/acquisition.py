# submodule Camera + submodule FPGA
import logging
from collections import Iterable
from typing import List, Tuple

from engine.common.config_manager import ConfigManager
from engine.modules.imaging.acquisition.acquisition_parameters import AcquisitionParameters
from engine.modules.imaging.acquisition.camera import Camera
from engine.modules.imaging.acquisition.trigger import Trigger

# noinspection PyUnresolvedReferences
from engine.modules.imaging.acquisition.gige_cam import GigeCam
# noinspection PyUnresolvedReferences
from engine.modules.imaging.acquisition.sim_cam import SimCam

# noinspection PyUnresolvedReferences
from engine.modules.imaging.acquisition.iprint_trigger import IprintTrigger
# noinspection PyUnresolvedReferences
from engine.modules.imaging.acquisition.sim_trigger import SimTrigger

logger = logging.getLogger(__name__)


# TODO implement link between trigger and camera, also for maximum fps, etc...

class Acquisition:
    CONFIG_PATH = 'image_acquisition'

    def __init__(self):
        super().__init__()
        self._config = ConfigManager(self.CONFIG_PATH)
        self._config.add_required_key([Camera.CONFIG_PATH, Trigger.CONFIG_PATH])
        self._triggers: List[Trigger] = []
        self._cameras: List[Camera] = []

    def set_config(self, config_manager, root_key):
        self._cameras = []
        self._triggers = []

        if self._config.set_config_from_parent(config_manager, root_key):  # if the config is valid
            # Triggers
            for i in range(len(self._config.get_property([Trigger.CONFIG_PATH]))):  # iterate over cameras list
                for t in Trigger.__subclasses__():  # compare with all available trigger subclasses if the type matches
                    if t.CONFIG_PATH in self._config.get_property([Trigger.CONFIG_PATH])[i]:
                        if self._config.get_property([Trigger.CONFIG_PATH, i, t.CONFIG_PATH])['enable'] is True:
                            trig: Trigger = t()
                            if trig.set_config(self._config, [Trigger.CONFIG_PATH, i, t.CONFIG_PATH]):
                                if trig.name in [t.name for t in self._triggers]:
                                    logger.error('Trigger name is not unique')
                                    return False
                                else:
                                    self._triggers.append(trig)
                            else:
                                return False  # error in config

            if not self._triggers:
                logger.error('No trigger found in config')

            # Cameras
            for i in range(len(self._config.get_property([Camera.CONFIG_PATH]))):  # iterate over cameras list
                for c in Camera.__subclasses__():  # compare with all available camera subclasses if the type matches
                    if c.CONFIG_PATH in self._config.get_property([Camera.CONFIG_PATH])[i]:
                        if self._config.get_property([Camera.CONFIG_PATH, i, c.CONFIG_PATH])['enable'] is True:
                            cam: Camera = c()
                            if cam.set_config(self._config, [Camera.CONFIG_PATH, i, c.CONFIG_PATH]):
                                if cam.name in [c.name for c in self._cameras]:
                                    logger.error('Camera name is not unique')
                                    return False  # camera name not unique
                                else:
                                    if cam.trigger_name in [t.name for t in
                                                            self._triggers]:  # check if camera trigger is existing
                                        cam.trigger = [t for t in self._triggers if t.name == cam.trigger_name][0]

                                        self._cameras.append(cam)
                                    else:
                                        logger.error("Camera trigger not found: " + cam.trigger_name)
                                        return False  # error
                            else:
                                return False  # error in cam config

            if not self._cameras:
                logger.error('No camera found in config')

            return True

        return False  # config not valid

    @property
    def cameras(self):
        """
        NOTE: This property should not be used except by the Analyzer classes
        :return:
        """
        return self._cameras

    def connect(self):
        """
        Connect to all HW and perform init. If any HW fails, stop everything.

        :return: True is successfully started, False otherwise
        """
        result = True

        if not self._config.is_valid:
            return False

        if not self._triggers or not self._cameras:
            return False  # we expect at least one camera and one trigger enabled

        for t in self._triggers:
            result &= t.open()

        if not result:
            self.disconnect()

        return result

    def disconnect(self):
        """
        Disconnect from all HW

        :return: True is successfully stopped, False otherwise
        """
        for t in self._triggers:
            t.close()

        for c in self._cameras:
            c.close()

        return True

    def get_camera_infos(self) -> List:
        """
        Returns the list of camera with their name, and more informations
        :return: list of tuple
        """
        return [{"name": c.name,
                 "trigger_name": c.trigger_name} for c in self._cameras]

    def get_parameters_range(self) -> List[Tuple[AcquisitionParameters, AcquisitionParameters]]:
        """
        Return the valid range for each parameter
        :return:
        """
        p = []
        for cam in self._cameras:
            p.append(cam.get_valid_parameters())
        return p

    def get_current_parameters(self) -> List[AcquisitionParameters]:
        """
        Return current parameters of each camera

        :return: List of Parameters, one for each camera
        """
        p = []
        for cam in self._cameras:
            p.append(cam.get_current_parameters())
        return p

    def set_parameters(self, param: AcquisitionParameters, camera=None, validation_only=False):
        """
        Set camera parameter(s) if valid.

        :param param: Acquisition.Parameters object
        :param camera: ID of the camera (starts from 0) or camera object. If None, set all cameras
        :param validation_only: if True, check only if the value is valid or not
        :return: True if the parameter is set successfully, False otherwise
        """
        cameras = self._get_cameras(camera)

        res = True

        for cam in cameras:
            res &= cam.set_parameters(param, validation_only)
        return res

    def start_trigger(self, camera=None, print_enable=True, frame_count=None):
        """
        Start triggering the LED and the Camera. And optionnal the print system

        :param camera: ID of the camera (starts from 0) or camera object. If None, set all cameras
        :param print_enable: True if the print system should also receive the trigger pulses
        :param frame_count: if specified (and supported by trigger), generate only the given amount frame
        :return: True if the parameter is set successfully, False otherwise
        """
        cameras = self._get_cameras(camera)

        res = True

        for cam in cameras:
            res &= cam.start_trigger(cam_frames=0, print_enable=print_enable)
        return res

    def stop_trigger(self, camera=None, ):
        """
        Stop trigger.

        :param camera: ID of the camera (starts from 0) or camera object. If None, set all cameras
        :return:
        """
        cameras = self._get_cameras(camera)

        res = True

        for cam in cameras:
            res &= cam.stop_trigger()
        return res

    def _get_cameras(self, camera):
        """
        Returns list of cameras. As input, supports camera index or camera object. Both as list or single element.
        :param camera:
        :return:
        """
        if isinstance(camera, Camera):
            cameras = [camera]
        elif isinstance(camera, list) and isinstance(camera[0], Camera):
            cameras = camera
        else:
            cam_idx = self._check_camera_id(camera)
            if not cam_idx:
                logger.warning("Invalid camera index")
                return False
            cameras = [self._cameras[c] for c in cam_idx]

        return cameras

    @staticmethod
    def list_available_cameras():
        """
        Discover all camera from all implemented interface (even if not enabled in the config).
        WARNING: this is not the current list of camera from the config files, but all the camera found by all the
        Camera subclasses

        :return: list of list of available cameras
        """
        cam_list = []
        for cam_class in Camera.__subclasses__():
            cam_list.append(cam_class.list_available_camera())
        return cam_list

    def _check_camera_id(self, camera_id):
        """
        Check if all camera id are valid. If yes return a list with all their ids

        :param camera_id: unique id or list of id
        :return: list of id if valid, None otherwise
        """
        if camera_id is None:
            return [*range(len(self._cameras))]

        if not isinstance(camera_id, Iterable):
            camera_id = [camera_id]
        cameras = list(camera_id)

        if not all([cam in range(len(self._cameras)) for cam in cameras]):
            return None  # invalid camera_id
        else:
            return cameras
