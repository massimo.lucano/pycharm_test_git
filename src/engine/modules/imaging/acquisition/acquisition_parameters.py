

class AcquisitionParameters:
    """
    Class storing all the parameters that are related to the acquisition (camera and trigger)
    """
    def __init__(self, led_delay_us=None, led_duration_us=None, double_strobe_delay_us=None, drops_per_img=None,
                 jetting_frequency=None, cam_frame_interval=None):
        self.led_delay_us = led_delay_us
        self.led_duration_us = led_duration_us
        self.double_strobe_delay_us = double_strobe_delay_us
        self.drops_per_img = drops_per_img
        self.jetting_frequency = jetting_frequency
        self.cam_frame_interval = cam_frame_interval
