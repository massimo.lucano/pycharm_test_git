import logging
import threading
import time

from engine.modules.imaging.acquisition.trigger import Trigger

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from engine.modules.imaging.acquisition.camera import Camera

from queue import Queue

logger = logging.getLogger(__name__)


# Make it a singleton so we can link both camera and trigger class without too many work
# @singleton # todo singleton decorator is not compatible with class attribute as it is
class SimTrigger(Trigger):
    """
    Simulation of trigger, works together with SimCam
    """

    CONFIG_PATH = 'sim'

    cam_trigger_eventqueue = Queue()
    encoder_trigger_eventqueue = Queue()

    class SimCamTriggerEvent:
        """
        Class to transfer all trigger information to the camera
        """
        def __init__(self, led_delay, led_duration, double_strobe_delay, drops_per_img):
            self.led_delay = led_delay
            self.led_duration = led_duration
            self.double_strobe_delay = double_strobe_delay
            self.drops_per_img = drops_per_img

    class SimEncoderEvent:
        """
        Class containing the information about the encoder simulation (1 encoder count = 1 drop)
        An event can contains multiples encoder pulse in order to emulate high frequency jetting without flooding the
        consumer with events
        """

        def __init__(self, encoder_pulses):
            self.encoder_pulses = encoder_pulses

    def __init__(self):
        super().__init__()
        self._is_open = False
        self._frequency = 1
        self._flash_delay = [0]
        self._flash_duration = [1]
        self._drops_per_img = [1]
        self._double_strobe_delay = [0]
        self._cam_divider = [1]
        self._cam_delay = [0]
        self._cam_duration = [2]

        self._cam_count = None
        self._trigger_started = threading.Event()
        self._printing = True
        # self._config.add_required_key(['images_path', 'fps_max'])  # camera specific only!
        # self._resolution = [self._config.get_property('resolution_x'), self._config.get_property('resolution_y')]
        # self._fps_max = self._config.get_property('fps_max')
        # self._images_path = self._config.get_property('images_path')

        self._stop_request = threading.Event()
        self._worker = threading.Thread(target=self.worker_thread, name='SimTrigger_worker')

    def worker_thread(self):
        """
        Separate thread emulating camera HW
        :return:
        """
        drop_counter = None
        cam_frame_counter = None

        while not self._stop_request.is_set():
            if False: # TODO enable later if required / self._trigger_started.is_set():  # generate cam trigger signals
                start = time.time()

                # count the current drop
                if drop_counter is None:
                    drop_counter = 0
                else:
                    drop_counter += self._cam_divider[0]

                if cam_frame_counter is None:
                    cam_frame_counter = 0
                else:
                    cam_frame_counter += 1

                # check if we have to stop generating drops
                new_drops = self._cam_divider[0]

                if self._cam_count:
                    if cam_frame_counter >= self._cam_count:
                        self._trigger_started.clear()
                        # new_drops -= (drop_counter - self._cam_count)

                # trigger new frame (give flash parameters)
                cam_ev = self.SimCamTriggerEvent(0, 1, 2, 1)
                self.cam_trigger_eventqueue.put(cam_ev)

                # update encoder # todo update a bit more often if possible, and not based on cam divider!
                encoder_ev = self.SimEncoderEvent(new_drops)
                self.encoder_trigger_eventqueue.put(encoder_ev)

                cam_period_s = self._cam_divider[0] / self._frequency
                elapsed = time.time() - start

                if elapsed > cam_period_s:
                    logger.warning(f'Thread period too slow for SimTrigger (or FPS too high!),'
                                   f'current: {elapsed}, target: {cam_period_s}')

                time.sleep(max(cam_period_s - elapsed, 0))
            else:
                drop_counter = None
                cam_frame_counter = None
                time.sleep(0.1)

    def open(self):
        self._is_open = True
        self._worker.start()
        return self._is_open

    def _start_acquisition(self, cam_frames=None, camera=None, print_enable=True):
        self._cam_frames = cam_frames
        self._trigger_started.set()
        return True

    def _stop_acquisition(self):
        self._trigger_started.clear()
        return True

    def is_acquiring(self):
        return self._trigger_started.is_set()

    def start_jetting(self):
        self._printing = True

    def _stop_jetting(self):
        self._printing = False

    def is_jetting(self):
        return self._printing

    def _get_jetting_frequency(self):
        return self._frequency

    def _set_jetting_frequency(self, frequency, validation_only=False):
        """
        Set the jetting frequency

        :param frequency:
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if not validation_only:
            self._frequency = frequency
        return True

    def get_camera_divider(self, camera: 'Camera'):
        return self._cam_divider[camera.trigger_idx]

    def _set_camera_divider(self, camera: 'Camera', divider, validation_only=False):
        """
        Set how often we take a picture of a drop

        :param divider:
        :param camera: 'Camera' object linked to the request
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if not validation_only:
            self._cam_divider[camera.trigger_idx] = divider
        return True

    def _set_camera_delay(self, camera: 'Camera', delay_us, validation_only=False):
        if not validation_only:
            self._cam_delay[camera.trigger_idx] = delay_us
        return True

    def get_camera_delay(self, camera: 'Camera'):
        return self._cam_delay[camera.trigger_idx]

    def set_camera_duration(self, camera: 'Camera', duration_us, validation_only=False):
        if not validation_only:
            self._cam_duration[camera.trigger_idx] = duration_us
        return True

    def get_camera_duration(self, camera: 'Camera'):
        return self._cam_duration[camera.trigger_idx]

    def get_flash_delay(self, camera: 'Camera'):
        return self._flash_delay[camera.trigger_idx]

    def get_flash_delay_range(self, camera: 'Camera'):
        return 0, 1000.0

    def _set_flash_delay(self, camera: 'Camera', delay_us, validation_only=False):
        """

        :param delay_us:
        :param validation_only: if True, check only if the value is valid or not
        :return:
        """
        if not validation_only:
            self._flash_delay[camera.trigger_idx] = delay_us
        return True

    def get_flash_duration(self, camera: 'Camera'):
        return self._flash_duration[camera.trigger_idx]

    def get_flash_duration_range(self, camera: 'Camera'):
        return 0.1, None

    def _set_flash_duration(self, camera: 'Camera', duration_us, validation_only=False):
        if not validation_only:
            self._flash_duration[camera.trigger_idx] = duration_us
        return True

    def get_drops_per_img(self, camera: 'Camera'):
        return self._drops_per_img[camera.trigger_idx]

    def get_drops_per_img_range(self, camera: 'Camera'):
        return 1, 10

    def _set_drops_per_img(self, camera: 'Camera', drops_per_img, validation_only=False):
        if not validation_only:
            self._drops_per_img[camera.trigger_idx] = drops_per_img
        return True

    def get_double_strobe_delay(self, camera: 'Camera'):
        return self._double_strobe_delay[camera.trigger_idx]

    def get_double_strobe_delay_range(self, camera: 'Camera'):
        return 0, 1000.0

    def set_double_strobe_delay(self, camera: 'Camera', double_strobe_delay_us=None, validation_only=False):
        if not validation_only:
            self._double_strobe_delay[camera.trigger_idx] = double_strobe_delay_us
        return True

    def close(self):
        self._is_open = False
        if self._worker.is_alive():
            self._stop_request.set()  # kill worker thread
            self._worker.join()
            self._stop_request.clear()
        return self._is_open
