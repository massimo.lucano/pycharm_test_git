#  Copyright (c) 2022. Droptimize

from engine.modules.motion.axis.axis import Axis


class DuetAxis(Axis):
    """

    """
    CONFIG_PATH = 'duet_axis'

    def __init__(self, parent_axis_system):
        super().__init__(parent_axis_system)

        self._config.add_required_key(['microsteps', 'steps_per_mm', 'motor_current'])

