#  Copyright (c) 2022. Droptimize

from engine.modules.motion.axis.axis import Axis


class SimAxis(Axis):
    CONFIG_PATH = 'sim_axis'

    def __init__(self, parent_axis_system):
        super().__init__(parent_axis_system)
