
#  Copyright (c) 2022. Droptimize

from abc import abstractmethod, ABC
from collections import namedtuple
from enum import IntEnum

from engine.common.config_manager import ConfigManager


class Axis(ABC):
    """
    Representation of a single axis. Mostly used to have an clear interface between the Motion module and the AxisSystem

    All configurations related to the Axis is also saved here.
    This class is also responsible to store the cache values of the axis (position, state, ...).
    """
    CONFIG_PATH = 'axis'

    AxisLimits = namedtuple('AxisLimits', ['position_min', 'position_max', 'speed_min', 'speed_max'])

    # States enum
    class State(IntEnum):
        UNKNOWN = 0
        IDLE = 1
        HOMING = 2
        READY = 3
        MOVING = 4
        ERROR = 5

    def __init__(self, parent_axis_system):
        super().__init__()
        self._config = ConfigManager(self.CONFIG_PATH)
        self._config.add_required_key(['enable', 'axis', 'hw_index', 'inverted', 'move_printhead',
                                       'min_pos_mm', 'max_pos_mm', 'max_speed_mm_s', 'max_accel_mm_s2'])

        self._axis_system = parent_axis_system

        # Cache values
        self._state: Axis.State = Axis.State.UNKNOWN
        self._calibrated = False
        self._position = None  # used to cache the position value

    def set_config(self, config_manager, root_key):
        return self._config.set_config_from_parent(config_manager, root_key)

    @property
    def config(self):
        return self._config

    @property
    def max_acceleration_mm_s2(self):
        return self._config.get_property(['max_accel_mm_s2'])

    @property
    def max_speed_mm_s(self):
        return self._config.get_property(['max_speed_mm_s'])

    @property
    def axis_letter(self):
        """
        Return the corresponding axis letter ('x', 'y', ...)
        :return:
        """
        return self._config.get_property(['axis'])

    @property
    def hw_index(self):
        """
        Return the corresponding output on the HW
        :return:
        """
        return self._config.get_property(['hw_index'])

    @property
    def position(self):
        """
        Returns the last cached position
        :return:
        """
        return self._position

    @position.setter
    def position(self, pos):
        self._position = pos  # todo add range check

    @property
    def state(self) -> State:
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    @property
    def is_inverted(self):
        """
        Check if we have to invert the direction of the axis
        :return:
        """
        return self._config.get_property(['inverted']) != self._config.get_property(['move_printhead'])

    @property
    def axis_limits(self) -> AxisLimits:
        """
        Return axis limits (position and speed)

        :return: namedtuple AxisLimits
        """
        return self.AxisLimits(position_min=self._config.get_property(['min_pos_mm']),
                               position_max=self._config.get_property(['max_pos_mm']),
                               speed_min=0,
                               speed_max=self._config.get_property(['max_speed_mm_s']))

    def is_moving(self):
        return self._state == Axis.State.MOVING

    def is_calibrated(self):
        return self._state > Axis.State.HOMING

    def check_if_acceleration_valid(self, acceleration):
        """
        Check if the acceleration is within a valid range. Raise an exception if not
        :param acceleration:
        :return:
        """
        if acceleration is None:
            raise ValueError("Invalid acceleration")

        if not (0.0 < acceleration <= self.max_acceleration_mm_s2):
            raise ValueError("Acceleration out of bounds")

        return True

    def check_if_speed_valid(self, speed):
        """
        Check if the speed is within a valid range. Raise an exception if not
        :param speed:
        :return:
        """
        if speed is None:
            raise ValueError("Invalid position")

        if not (0.0 < speed <= self._config.get_property(['max_speed_mm_s'])):
            raise ValueError("Speed out of bounds")

        return True

    def check_if_position_valid(self, position):
        """
        Check if the position is within a valid range. Raise an exception if not
        :param position:
        :return:
        """
        if position is None:
            raise ValueError("Invalid position")

        if not (self._config.get_property(['min_pos_mm']) <= position <= self._config.get_property(['max_pos_mm'])):
            raise ValueError("Position out of bounds")

        return True
