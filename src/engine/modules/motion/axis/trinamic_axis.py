#  Copyright (c) 2022. Droptimize

from engine.modules.motion.axis.axis import Axis


class TrinamicAxis(Axis):
    CONFIG_PATH = 'trinamic_axis'

    def __init__(self, parent_axis_system):
        super().__init__(parent_axis_system)
        self._config.add_required_key(['motor_current', 'turns_per_mm', 'home_speed'])

