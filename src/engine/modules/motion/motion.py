#  Copyright (c) 2022. Droptimize

import logging
import time
from enum import Enum
from typing import Optional

import numpy as np
import threading

from engine.common.coordinates import Coordinate
from engine.modules.module import Module

# TODO do not forget to add all subclasses here below:
from engine.modules.motion.axis_system.axis_system import AxisSystem
# noinspection PyUnresolvedReferences
from engine.modules.motion.axis_system.sim_axis_system import SimAxisSystem
# noinspection PyUnresolvedReferences
from engine.modules.motion.axis_system.trinamic_axis_system import TrinamicAxisSystem
# noinspection PyUnresolvedReferences
from engine.modules.motion.axis_system.duet_axis_system.duet_axis_system import DuetAxisSystem


logger = logging.getLogger(__name__)


class Motion(Module):
    """
    Managing all axis (x, y, z) moving the printhead or the camera

    This class will offer high level method to move the axis (i.e. moving the camera in front of a specific nozzle).

    It compensate also for axis error (scaling, rotation, translation, ...) via a correction matrix.
    Virtual coordinates are the ideal coordinates, used in all motion interfaces to the engine (0,0,0 = first nozzle)
    Real coordinates are the compensated values used by the axis systems themselves.

    Primary axis define the main axis system where the camera and the printhead are referenced.
    Auxiliary axis are used to perform side tasks, i.e. printing axis, cleaning axis, ... and are driven separately

    It supports also one additional axis for printing (no compensation), named 'u'.

    """
    _VERSION = '0.1'

    # list of all secondary axis and their related letters
    class SecondaryAxis(Enum):
        PRINT_AXIS = 'u'

    def __init__(self):
        super().__init__('Motion Module', self._VERSION)

        modules_keys = [AxisSystem.CONFIG_PATH, 'correction_matrix']
        self._module_config.add_required_key(modules_keys)

        self._axis_system = []  # list of axis system

        # observer pattern for motion changes
        self._notifier_lock = threading.RLock()
        self._last_is_moving_state = Coordinate()  # save the last is_moving change (used for notification purpose)
        self._last_position = Coordinate()
        self._is_moving_listeners = []  # list of functions to call when is_moving changes, arg will be new is_moving value

        # axis interfaces
        self._axis = Coordinate(None, None, None)
        self._print_axis = None  # additional axis, only used for print tests

        self._correction_matrix = np.identity(4)  # transformation matrix (see: https://www.brainvoyager.com/bv/doc/UsersGuide/CoordsAndTransforms/SpatialTransformationMatrices.html#:~:text=A%20rotation%20matrix%20rotates%20an,needed%20to%20express%20complex%20rotations.&text=This%20is%20commonly%20the%20case%20for%20right%2Dhanded%20coordinate%20systems.)
        self._inv_correction_matrix = np.identity(4)

        # self._current_request = None  # TODO draft of a request management system, can be improved or generalized for other class as well
        # self._request_counter = 0

    @property
    def current_position(self) -> Coordinate:
        """
        Get the current position

        :return: Coordinate object with the current position or None if the position is not known or no axis present
        """
        pos = [a.position if a else None for a in self._axis]
        return Coordinate.from_list(pos)

    @property
    def min_position(self) -> Coordinate:
        """
        Return min position

        :return:
        """
        positions = [a.axis_limits.position_min if a else None for a in self._axis]
        return Coordinate.from_list(positions)

    @property
    def max_position(self) -> Coordinate:
        """
        Return max position

        :return:
        """
        positions = [a.axis_limits.position_max if a else None for a in self._axis]
        return (Coordinate.from_list(positions))


    @property
    def max_speed(self) -> Coordinate:
        """
        Return max speed in mm/s

        :return:
        """
        speeds = [a.axis_limits.speed_max if a is not None else None for a in self._axis]
        return speeds#(Coordinate.from_list(speeds), is_vector=True)

    @property
    def is_moving(self) -> Coordinate:
        """
        Indicates whether the axis is moving or not. The flag is set to True as soon as a move command is started.

        :return: Coordinate with True if moving, False if not moving.
        """
        moving = [a.is_moving() if a is not None else False for a in self._axis]
        return Coordinate.from_list(moving)

    @property
    def is_calibrated(self) -> Coordinate:
        """
        Indicates whether the axis is calibrated or not.

        :return: Coordinate with True if calibrated, False otherwise
        """
        cal = [a.is_calibrated() if a is not None else True for a in self._axis]
        return Coordinate.from_list(cal)

    def _update_module_config(self):
        # Transformation matrix
        self.correction_matrix = np.array(self._module_config.get_property(['correction_matrix']))

        # Axis system
        for idx, system in enumerate(self._module_config.get_property([AxisSystem.CONFIG_PATH])):  # list of axis_system
            for a in AxisSystem.__subclasses__():  # list all axis system subclasses
                if a.CONFIG_PATH in system.keys():
                    if system[a.CONFIG_PATH]['enable'] is True:
                        logger.info(f'Instantiating axis system {a.__name__}')
                        a_inst = a()
                        a_inst.set_moving_observer(self.is_moving_notifier)
                        if not a_inst.set_config(self._module_config, [AxisSystem.CONFIG_PATH, idx, a.CONFIG_PATH]):
                            logger.error("Error in config while instantiating axis")
                            return False

                        # todo manage conflicts if more than one axis
                        try:
                            if a_inst.axis.x is not None:
                                if self._axis.x is None:
                                    self._axis.x = a_inst.axis.x
                                else:
                                    raise AttributeError("X axis already assigned")
                            if a_inst.axis.y is not None:
                                if self._axis.y is None:
                                    self._axis.y = a_inst.axis.y
                                else:
                                    raise AttributeError("Y axis already assigned")
                            if a_inst.axis.z is not None:
                                if self._axis.z is None:
                                    self._axis.z = a_inst.axis.z
                                else:
                                    raise AttributeError("Z axis already assigned")

                            # secondary axis
                            if 'u' in a_inst.secondary_axis:
                                if self._print_axis is None:
                                    self._print_axis = a_inst.secondary_axis['u']
                                else:
                                    raise AttributeError("U axis already assigned")
                        except AttributeError as e:
                            logger.error(f"Duplicate axis in configuration, please check the config file. " + str(e))
                            return False

                        self._axis_system.append(a_inst)
        return True

    def _start(self):
        """
        Start motion modules, connect all axis, calibrate them. If any axis fails, stop everything.

        :return: True if all axis are started and calibrated
        """
        result = True

        for sys in self._axis_system:
            if sys:
                result &= sys.connect()

        result &= self.calibrate(wait_on_completion=True)

        if result:
            self._state = self.State.READY
        else:
            self._stop()

        return result

    def _stop(self):
        """
        Stop motion modules, cut axis power. Disconnect them.

        :return:
        """
        self.move_abort()

        for sys in self._axis_system:
            if sys:
                sys.stop()
                sys.disconnect()

        return True

    def emergency_stop(self):
        """
        Emergency stop. Cut all axis power.

        :return:
        """
        for sys in self._axis_system:
            if sys:
                sys.emergency_stop()

    def calibrate(self, force=False, wait_on_completion=False):  # todo not sure if we should allow a blocking call here
        """
        Calibrate axis. Reset axis and perform homing sequence, axis-specific.
        Automatically called during start.

        :param force: force calibration even if axis has its calibrated flag set
        :param wait_on_completion: if True, wait until calibration is done for all axis. if False (default),
        returns immediately.
        :return:
        """
        result = True

        for sys in self._axis_system:
            if sys:
                result &= sys.calibrate(force=force)

        if wait_on_completion:
            polling = 1
            timeout_secs = 90  # TODO set constant somewhere

            while timeout_secs > 0:
                if False not in self.is_calibrated:
                    return True

                time.sleep(polling)
                timeout_secs -= polling
            logger.error("Timeout : Failed to calibrate axis...")
            return False  # timeout
        else:
            return result

    def move_to(self, target_pos: Coordinate, speed: Optional[Coordinate] = None):
        """
        Move to the target position at the given speed.

        :param target_pos:
        :param speed:
        :return: True if the command is accepted by all axis, False otherwise
        """
        if not speed:
            speed = Coordinate()

        result = True

        # Send movement to all axis system
        for sys in self._axis_system:
            try:
                result &= sys.move_to(target_pos, speed)
            except ValueError as err:
                logger.error(f"Error while moving to. ValueError: {err}")
                result &= False

        return result

    def move_by(self, offset_pos: Coordinate, speed: Optional[Coordinate] = None):
        """
        Move by the given offset at the given speed.

        :param offset_pos:
        :param speed:
        :return: True if the command is accepted by all axis, False otherwise
        """
        target_pos = self.current_position + offset_pos
        return self.move_to(target_pos, speed)

    def move_print_axis_to(self, target_pos: float, speed: float):
        """
        Move the print axis

        :param target_pos:
        :param speed:
        :return:
        """
        if self._print_axis is None:
            logger.error("Cannot move print axis if not specified")
            return False

        result = True

        # Send movement to all axis system
        for sys in self._axis_system:
            try:
                result &= sys.move_secondary_axis_to(self.SecondaryAxis.PRINT_AXIS.value, target_pos, speed)
            except ValueError as err:
                logger.error(f"Error while moving to. ValueError: {err}")
                result &= False

        return result

    def move_abort(self):
        result = True

        for sys in self._axis_system:
            result &= sys.stop()

        return result

    def set_position(self, position: Coordinate):
        """
        Update current position

        :param position: Coordinate object with current position or None to skip the update
        :return: True is all position were set fine. False otherwise
        """
        result = True

        for sys in self._axis_system:
            try:
                result &= sys.set_position((position))
            except ValueError as err:
                logger.error(f"Error while setting position. ValueError: {err}")
                result &= False

        return result

    def set_speed(self, speed: Coordinate):
        """
        Update current speed

        :param speed: Coordinate object with current position or None to skip the update
        :return: True is all speed were set fine. False otherwise
        """
        result = True

        for sys in self._axis_system:
            try:
                result &= sys.set_speed(speed) #(speed, is_vector=True))
            except ValueError as err:
                logger.error(f"Error while setting speed. ValueError: {err}")
                result &= False

        return result

    def subscribe_for_is_moving(self, fun):
        """
        Register a function to call when is_moving flag is changing. Function will be call with first argument = new
        is_moving value.

        :param fun:
        :return:
        """
        self._is_moving_listeners.append(fun)

    def unsubscribe_for_is_moving(self, fun):
        """
        Remove listener

        :param fun:
        :return:
        """
        self._is_moving_listeners.remove(fun)

    def is_moving_notifier(self):
        """
        Receives notifications from axis systems when their is_moving state changes. Check all states and notify further is we have a global is_moving change on module level.

        :return:
        """
        with self._notifier_lock:  # could be a bottleneck but no real other solution for now...
            state = self.is_moving

            # change of moving state or position while not moving (because of small movement)
            if (state != self._last_is_moving_state) or (state == Coordinate(False, False, False) and self._last_position != self.current_position):
                self._last_is_moving_state = state

                # notify listeners
                for lis in self._is_moving_listeners:
                    lis(state, self.current_position)

            self._last_position = self.current_position

    @staticmethod
    def _check_for_collision(cls, origin_pos: Coordinate, target_pos: Coordinate):
        """
        **TO BE IMPLEMENTED**
        Check if collision while moving from origin to target position.

        :param origin_pos:
        :param target_pos:
        :return:
        """
        return Coordinate(False, False, False)
