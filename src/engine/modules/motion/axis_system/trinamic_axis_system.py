#  Copyright (c) 2022. Droptimize

import logging
from collections import namedtuple
from typing import Optional, List

import threading

from PyTrinamic.TMCL import TMCL_Command
from PyTrinamic.connections.ConnectionManager import ConnectionManager

from engine.modules.motion.axis.axis import Axis
from engine.modules.motion.axis.trinamic_axis import TrinamicAxis
from engine.modules.motion.axis_system.axis_system import AxisSystem

logger = logging.getLogger(__name__)


class TrinamicAxisSystem(AxisSystem):
    """
    Implement a trinamic TMCL module interface.
    """

    CONFIG_PATH = 'trinamic_tmcl_system'

    _THREAD_PERIOD_S = 0.02
    _MAX_MOTORS_COUNT = 1

    ModuleConfig = namedtuple('TMCL_config',
                              ['name', 'current', 'pulse_divisor', 'ramp_divisor', 'microsteps', 'acceleration',
                               'speed'])

    # Config per module:
    modules_config = [ModuleConfig('TMCM_1141', 2.0, 2, 13, 8, 2047, 2047),
                      # todo make pulse divisor and microsteps calc. for optimum performances instead.
                      ModuleConfig('TMCM_1161', 2.8, 15, 13, 8, 2047, 2047)]

    def __init__(self):
        super().__init__()
        self._config.add_required_key(['serial', 'module_type'])

        self._current_module_config: Optional[TrinamicAxisSystem.ModuleConfig] = None

        self.GPs = _GPs
        self.APs = _APs
        self.ENUMs = _ENUMs

        # todo add rlock
        self._hw_lock = threading.RLock()
        self._connectionManager = None
        self._interface = None
        self._module = None

        self._homed = [False] * self.max_motors_count

    @property
    def supported_axis_classes(self):
        return [TrinamicAxis]

    @property
    def max_motors_count(self):
        return self._MAX_MOTORS_COUNT

    def _configure(self):
        module_type = self._config.get_property(['module_type'])

        if module_type not in [m.name for m in self.modules_config]:
            logger.error(f"Unknown module type: {module_type}")
            return False

        self._current_module_config = next(m for m in self.modules_config if m.name == module_type)
        return True

    @property
    def _turns_per_mm(self):
        param = [None] * self.max_motors_count

        for a in self._axis:
            if a is not None:
                param[self._axis_index[a]] = a.config.get_property(['turns_per_mm'])
        return param

    def _connect(self):
        args = ['--port "/dev/ttyACM0"']  # todo check if correct, if yes read port from config?

        self._homed = [False] * self.max_motors_count

        with self._hw_lock:
            self._connectionManager = ConnectionManager(argList=args,
                                                        connectionType='usb_tmcl')  # todo check if we need common connectionmanager (class-wide)
            self._interface = self._connectionManager.connect()

            # configure it

            """
             IMPORTANT: following settings must be performed prior to usage     *
            %*          SET: upper limit for speed           axis parameter 4          *
            %*          SET: upper limit for acceleration    axis parameter 5          *
            %*          SET: upper limit for current         axis parameter 6          *
            %*          SET: microstep resolution            axis parameter 140        *
            %*          SET: disconnect or connect pullups according to your endstop   
            """
            self.stop()

            # todo use these parameters to define microsteps and pulse_divisor + ramp divisor
            # self._config.get_property(['max_speed_mm_s'])
            # self._config.get_property(['max_accel_mm_s2'])

            # configure module
            for axis in self._axis:
                if axis is None:
                    continue

                motor = self._axis_index[axis]

                self._set_axis_parameter(self.APs.TargetPosition, 0, motor)
                self._set_axis_parameter(self.APs.ActualPosition, 0, motor)

                self._set_max_current(axis.config.get_property(['motor_current']), motor)
                self._set_motor_standby_current(axis.config.get_property(['motor_current']) / 2.0, motor)
                if axis.config.get_property('home_speed') > 0.0:
                    self._configure_reference(axis.config.get_property(['home_speed']), motor)
                self._set_axis_parameter(self.APs.PulseDivisor, self._current_module_config.pulse_divisor, motor)
                self._set_axis_parameter(self.APs.MicrostepResolution, self._current_module_config.microsteps, motor)

            return True

    def _stop(self):
        try:
            for i in self._active_axis_hw_index:
                self._interface.stop(i)
        except:
            pass
        return True

    def _disconnect(self):
        with self._hw_lock:
            self._interface.close()

        return True

    def set_axis_power(self, enable):
        pass

    def _get_position(self, index=None):  # todo implement for more than 1 axis
        pos = [None] * self.max_motors_count
        for i in self._active_axis_hw_index:
            driver_pos = self._get_axis_parameter(self.APs.ActualPosition, i)
            pos[i] = driver_pos / 200 / 2 ** self._current_module_config.microsteps / self._turns_per_mm[i]
        return pos

    def _set_position(self, position):
        for idx, p in enumerate(position):
            if p is None:
                continue
            if idx in self._active_axis_hw_index:
                driver_pos = int(p * 200 * 2 ** self._current_module_config.microsteps * self._turns_per_mm[idx])
                self._set_axis_parameter(self.APs.ActualPosition, driver_pos, idx)
        return True

    def _move_to(self, position):
        for idx, p in enumerate(position):
            if idx in self._active_axis_hw_index:
                driver_pos = int(p * 200 * 2 ** self._current_module_config.microsteps * self._turns_per_mm[idx])
                self._interface.move(0, idx, driver_pos)
                # self._set_axis_parameter(self.APs.TargetPosition, driver_pos)
                # self._set_ramp_mode(0)
        return True

    def _calibrate(self, cal, force=False):
        for idx, c in enumerate(cal):
            if c is True and idx in self._active_axis_hw_index:
                if self._inv_axis_index[idx].config.get_property('home_speed') > 0.0:
                    self._interface.send(TMCL_Command.RFS, 0, idx, 0)
                    # todo monitor end of homing
                    self._homed[idx] = False
                else:
                    self._homed[idx] = True
        return True

    def emergency_stop(self):
        self.stop()

    def _set_acceleration(self, acceleration) -> (bool, float):
        # todo check boundaries (raise ValueError in super().set_acc..?) + convert and send
        pass

    def _set_speed(self, speed):
        """
        Set the target speed in mm/s
        :param speed:
        :return:
        """
        for idx, s in enumerate(speed):
            if s is not None and idx in self._active_axis_hw_index:
                # todo multiple motors not supported here
                velocity = self._velocity_to_internal_unit(s, idx)
                self._set_axis_parameter(self.APs.MaxVelocity, velocity, idx)
        return True

    def _get_axis_state(self, index=None) -> List:  # todo improve it!
        states = [Axis.State.UNKNOWN] * self.max_motors_count
        for i in self._active_axis_hw_index:
            if not self._homed[i]:
                states[i] = Axis.State.UNKNOWN
            elif self._is_moving(i):
                states[i] = Axis.State.MOVING
            else:
                states[i] = Axis.State.READY
        return states

    def _is_moving(self, hw_index):
        return self._get_axis_parameter(self.APs.ActualVelocity, hw_index) != 0
        # return not self._get_axis_parameter(self.APs.PositionReachedFlag, hw_index)

    # Axis parameter access
    def _get_axis_parameter(self, apType, motor):
        with self._hw_lock:
            return self._interface.axisParameter(apType, motor)

    def _set_axis_parameter(self, apType, value, motor):
        with self._hw_lock:
            self._interface.setAxisParameter(apType, motor, int(value))

    # Global parameter access
    def _get_global_parameter(self, gpType, bank):
        with self._hw_lock:
            return self._interface.globalParameter(gpType, bank)

    def _set_global_parameter(self, gpType, bank, value):
        with self._hw_lock:
            self._interface.setGlobalParameter(gpType, bank, value)

    # Current control functions
    def _set_motor_standby_current(self, current, motor):
        """
        Set standstill current

        :param current: in A
        :return:
        """
        value = self._current_to_internal_unit(current)
        self._set_axis_parameter(self.APs.StandbyCurrent, value, motor)

    def _set_max_current(self, current, motor):
        """
        Set maximum current

        :param current: in A
        :return:
        """
        value = self._current_to_internal_unit(current)
        self._set_axis_parameter(self.APs.MaxCurrent, value, motor)

    def _current_to_internal_unit(self, current):
        """
        Convert current in A to internal value.
        Raise ValueError if the current is not in driver range

        :param current:
        :return: driver internal value
        """
        if current > self._current_module_config.current:
            raise ValueError(f"Current higher than maximum allowed of {self._current_module_config.current}")
        else:
            value = current * 255 / self._current_module_config.current
            if value == 0:
                raise ValueError(
                    f"Current lower than minimum acceptable value of {self._current_module_config.current / 255}")
            else:
                return value

    def _velocity_to_internal_unit(self, speed, motor):
        """
        Convert speed value parameter (0...2047) based on motor settings.
        See TMCM-1141_TMCL_firmware_manual_Fw1.42_Rev1.09.pdf p.90

        :param speed: target speed in mm/s
        :return: velocity to send to the motor driver
        """
        turns_per_s = speed * self._turns_per_mm[motor]
        microsteps_per_s = turns_per_s * 200 * 2 ** self._current_module_config.microsteps
        int_speed = int(microsteps_per_s * 2 ** self._current_module_config.pulse_divisor * 2048 * 32 / 16e6)

        if 0 < abs(
                int_speed) < self._current_module_config.speed:  # todo test what we get when setting negative speed in positionning mode
            return int_speed
        else:
            raise ValueError(f"Target speed not allowed")

    # Homing functions
    def _configure_reference(self, speed, motor, mode=1):
        home_speed = self._velocity_to_internal_unit(speed, motor)
        self._set_axis_parameter(self.APs.ReferenceSearchMode, mode, motor)  # left only
        self._set_axis_parameter(self.APs.ReferenceSearchSpeed, home_speed, motor)  # set speed
        self._set_axis_parameter(self.APs.referenceSwitchSpeed, home_speed, motor)
        return True


class _APs:
    TargetPosition = 0
    ActualPosition = 1
    TargetVelocity = 2
    ActualVelocity = 3
    MaxVelocity = 4
    MaxAcceleration = 5
    MaxCurrent = 6
    StandbyCurrent = 7
    PositionReachedFlag = 8
    referenceSwitchStatus = 9
    RightEndstop = 10
    LeftEndstop = 11
    rightLimitSwitchDisable = 12
    leftLimitSwitchDisable = 13
    minimumSpeed = 130
    actualAcceleration = 135
    RampMode = 138
    MicrostepResolution = 140
    Ref_SwitchTolerance = 141
    softStopFlag = 149
    EndSwitchPowerDown = 150
    rampDivisor = 153
    PulseDivisor = 154
    Intpol = 160
    DoubleEdgeSteps = 161
    ChopperBlankTime = 162
    ConstantTOffMode = 163
    DisableFastDecayComparator = 164
    ChopperHysteresisEnd = 165
    ChopperHysteresisStart = 166
    TOff = 167
    SEIMIN = 168
    SECDS = 169
    smartEnergyHysteresis = 170
    SECUS = 171
    smartEnergyHysteresisStart = 172
    SG2FilterEnable = 173
    SG2Threshold = 174
    slopeControlHighSide = 175
    slopeControlLowSide = 176
    ShortToGroundProtection = 177
    ShortDetectionTime = 178
    VSense = 179
    smartEnergyActualCurrent = 180
    smartEnergyStallVelocity = 181
    smartEnergyThresholdSpeed = 182
    smartEnergySlowRunCurrent = 183
    RandomTOffMode = 184
    ReferenceSearchMode = 193
    ReferenceSearchSpeed = 194
    referenceSwitchSpeed = 195
    referenceSwitchDistance = 196
    lastReferenceSwitchPosition = 197
    BoostCurrent = 200
    freewheelingDelay = 204
    LoadValue = 206
    extendedErrorFlags = 207
    DrvStatusFlags = 208
    EncoderPosition = 209
    EncoderResolution = 210
    max_EncoderDeviation = 212
    PowerDownDelay = 214
    absoluteResolverValue = 215
    Step_DirectionMode = 254
    CurrentStepping = 0


class _ENUMs:
    FLAG_POSITION_END = 0x00004000


class _GPs:
    timer_0 = 0
    timer_1 = 1
    timer_2 = 2
    stopLeft_0 = 27
    stopRight_0 = 28
    input_0 = 39
    input_1 = 40
    input_2 = 41
    input_3 = 42
    serialBaudRate = 65
    serialAddress = 66
    ASCIIMode = 67
    serialHeartbeat = 68
    telegramPauseTime = 75
    serialHostAddress = 76
    autoStartMode = 77
    limitSwitchPolarity = 79
    protectionMode = 81
    eepromCoordinateStore = 84
    zeroUserVariables = 85
    serialSecondaryAddress = 87
    reverseShaftDirection = 90
    applicationStatus = 128
    downloadMode = 129
    programCounter = 130
    lastTmclError = 131
    tickTimer = 132
    randomNumber = 133
    Intpol = 255
