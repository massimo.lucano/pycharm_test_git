#  Copyright (c) 2022. Droptimize

import logging
from collections import namedtuple
from typing import Optional, List

import threading

from engine.modules.motion.axis.axis import Axis
from engine.modules.motion.axis.duet_axis import DuetAxis
from engine.modules.motion.axis_system.axis_system import AxisSystem
from engine.modules.motion.axis_system.duet_axis_system.duet_web_api import DuetWebAPI

logger = logging.getLogger(__name__)

logging.getLogger('requests').setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


class DuetAxisSystem(AxisSystem):
    """
    Implement a trinamic TMCL module interface.
    """
    CONFIG_PATH = 'duet_system'

    _THREAD_PERIOD_S = 0.02
    _MAX_MOTORS_COUNT = 5
    _DEFAULT_SPEED_MM_S = 10.0

    BoardConfig = namedtuple('board_config',
                             ['name', 'firmware_version', 'motor_count', 'axis_name', 'max_current', 'current_unit_to_mA', 'acceleration', 'speed'])  # todo define real config here

    # Config per board:
    boards_config = [BoardConfig(name='Duet2Wifi', firmware_version=2, motor_count=3, axis_name=['X', 'Y', 'Z', 'U', 'V'],  # TODO complete that
                                 max_current=2.5, current_unit_to_mA=100.0, acceleration=None, speed=None),
                     BoardConfig(name='DuetMaestro', firmware_version=None, motor_count=None, axis_name=[None],
                                  max_current=None, current_unit_to_mA=50.0, acceleration=None, speed=None),
                     BoardConfig(name='Duet3_MB6HC', firmware_version=None, motor_count=None, axis_name=[None],
                                  max_current=None, current_unit_to_mA=26.2, acceleration=None, speed=None),
                     BoardConfig(name='Duet3_EXP3HC', firmware_version=None, motor_count=None, axis_name=[None],
                                  max_current=None, current_unit_to_mA=26.2, acceleration=None, speed=None),
                     BoardConfig(name='Duet3 Mini5+', firmware_version=None, motor_count=None, axis_name=[None],
                                  max_current=None, current_unit_to_mA=74.0, acceleration=None, speed=None),
                     ]

    def __init__(self):
        super().__init__()
        self._config.add_required_key(['api_address', 'board_type', 'current_idle_factor']) #

        self._current_board_config: Optional[DuetAxisSystem.BoardConfig] = None

        # todo add rlock
        self._hw_lock = threading.RLock() # TODO check if used everywhere
        self._interface = None
        self._module = None

        self._homed = [False] * self.max_motors_count
        self._temp_speed = None  # save temporary the speed values until we start a movement

        self._temp_speed_mm_min = [None] * self.max_motors_count

    @property
    def supported_axis_classes(self):
        return [DuetAxis]

    @property
    def max_motors_count(self):
        """
        Maximum number of motors supported by the board
        :return:
        """
        if self._current_board_config is not None:
            return self._current_board_config.motor_count
        else:
            return self._MAX_MOTORS_COUNT

    def _idx_to_letter(self, idx):
        """
        Convert internal axis index to internal letter (used in GCode and other commands)
        :param idx: the HW index
        :return: The letter representing the axis ('X', 'Y', ...)
        """
        return self._current_board_config.axis_name[idx]

    def _configure(self):
        """
        Perform configuration once the config file is applied.

        :return: True if config is valid
        """
        board_type = self._config.get_property(['board_type'])

        if board_type not in [m.name for m in self.boards_config]:
            logger.error(f"Unknown module type: {board_type}")
            return False

        self._current_board_config = next(m for m in self.boards_config if m.name == board_type)
        return True

    def _connect(self):
        """
        Connect to the board, check if firmware is correct, perform all initial configuration (microsteps, current, ...)
        :return:
        """
        self._homed = [False] * self.max_motors_count
        self._temp_speed_mm_min = [None] * self.max_motors_count  # default speed

        with self._hw_lock:
            self._interface = DuetWebAPI(self._config.get_property(['api_address']), self._config.get_property(['api_password']))
            self._interface.connect()  #TODO add return value and checks

            if self._interface.printerType() != self._current_board_config.firmware_version:
                logger.error("Wrong firmware version detected, abort connection...")
                self._interface = None
                return False

            # TODO IMPORTANT: Driver mapping: M584: Set drive mapping : https://duet3d.dozuki.com/Wiki/Gcode#Section_M584_Set_drive_mapping
            #
            # TODO temp. remove to avoid re-homing
            # # Ratio between motor current while moving and in idle. Same for all axis
            # self._interface.gCode(f'M906 I{self._config.get_property(["current_idle_factor"])}')
            #
            # # Configure motors-specific values
            # for axis in self._axis:
            #     if axis is None:
            #         continue
            #
            #     motor_idx = self._axis_index[axis]
            #     microsteps = axis.config.get_property(['microsteps'])
            #     motor_current = axis.config.get_property(['motor_current'])
            #     steps_per_mm = axis.config.get_property(['steps_per_mm'])
            #
            #     # Microstepping
            #     if microsteps not in [2**n for n in range(9)]:
            #         logger.error(f"Invalid microstep setting: {microsteps}")
            #         return False
            #     else:
            #         logger.debug(f"Set DuetAxis {self._idx_to_letter(motor_idx)} to microstep 1/{microsteps}")
            #         self._interface.gCode(f'M350 ' + self._idx_to_letter(motor_idx) + str(microsteps))
            #
            #     # Motor current
            #     if not(0 < motor_current <= self._current_board_config.max_current):
            #         logger.error(f"Motor current out of range: {motor_current} A")
            #         return False
            #     else:
            #         current_unit = 1000.0*motor_current
            #         logger.debug(f"Set DuetAxis {self._idx_to_letter(motor_idx)} current at {motor_current} ({current_unit})")
            #         self._interface.gCode(f'M906 ' + self._idx_to_letter(motor_idx) + str(current_unit))
            #
            #     # Axis scaling  # TODO we have also to configure the U axis in case of dual-axis Z
            #     self._interface.gCode(f'M92 ' + self._idx_to_letter(motor_idx) + str(steps_per_mm*microsteps))

                # Max acceleration
                self._interface.gCode(f'M201 ' + self._idx_to_letter(motor_idx) + str(axis.max_acceleration_mm_s2))

                # Max speed
                self._interface.gCode(f'M203 ' + self._idx_to_letter(motor_idx) + str(axis.max_speed_mm_s*60))

                # todo set axis limit: M208 S0/S1

            # Overall acceleration (RepRap is anyway limiting to the slowest acceleration of any axis - we could also set it higher)
            max_accel = max([axis.max_acceleration_mm_s2 for axis in self._axis if axis is not None])
            self._interface.gCode(f'M204 P{max_accel} T{max_accel}')

            # Unit to mm, machine coordinates, first coordinate system, absolute positionning
            self._interface.gCode("G21 G53 G54 G90")

            # Set default speed (minimum 0.5mm/s:
            self._interface.gCode(f"G0 F{max([0.5, min([axis.max_speed_mm_s for axis in self._axis if axis is not None])*60])}")

            # TODO homing-related stuffs (direction, speed, ...)

        return True

    def _stop(self):
        """
        Stop after finishing the movement
        :return:
        """
        self._interface.gCode('M0')
        return True

    def _disconnect(self):
        with self._hw_lock:
            self._interface = None
        return True

    def set_axis_power(self, enable):
        pass

    def _get_position(self, index=None):
        pos = [None] * self.max_motors_count
        d = self._interface.getCoords()
        for i in self._active_axis_hw_index:
            pos[i] = d[self._current_board_config.axis_name[i]]
        return pos

    def _set_position(self, position):
        if len(position) == 0:
            logger.warning("Empty set position, skip it")
            return True

        target_pos = {}
        for idx, p in enumerate(position):
            if p is not None and idx in self._active_axis_hw_index:
                target_pos[self._current_board_config.axis_name[idx]] = int(p)
        cmd = 'G92 ' + ' '.join([f'{axis}{pos}' for axis, pos in target_pos])
        self._interface.gCode(cmd)
        logger.debug(f'Sent "{cmd}" to Duet')

        return True

    def _move_to(self, position):
        if len(position) == 0:
            logger.warning("Empty move to position, skip it")
            return True

        target_pos = {}
        travel_time = [0.0] * self.max_motors_count
        current_pos = self._get_position()

        for idx, p in enumerate(position):
            if p is None:
                continue
            if idx in self._active_axis_hw_index:
                target_pos[self._current_board_config.axis_name[idx]] = float(p)
                travel_time[idx] = (p - current_pos[idx]) if current_pos is not None else 0.0

        # Take the longest travel time as the reference for the overall speed of the coordinated movement.
        speed = self._temp_speed_mm_min[travel_time.index(max(travel_time))]
        if speed is None:
            logger.warning("Cannot compute travel speed (never set), using default speed instead")
            speed = self._DEFAULT_SPEED_MM_S*60

        cmd = 'G1 ' + ' '.join([f'{axis}{pos}' for axis, pos in target_pos.items()]) + f' F{speed}'
        self._interface.gCode(cmd)
        logger.debug(f'Sent "{cmd}" to Duet')
        return True

    def _calibrate(self, cal, force=False):
        axis_list = []
        for idx, c in enumerate(cal):
            if c is True and idx in self._active_axis_hw_index:
                if not self._homed[idx] or force:
                    axis_list.append(self._current_board_config.axis_name[idx])
                # todo monitor end of homing somewhere?

        if axis_list:
            cmd = 'G28 ' + ' '.join(axis_list)
            self._interface.gCode(cmd)
            logger.debug(f'Sent "{cmd}" to Duet')
        else:
            logger.debug("All axis already homed.")
        return True

    def emergency_stop(self):
        """
        Immediate stop. Board has to be reset to continue

        :return:
        """
        self._interface.gCode('M112')

    def _set_acceleration(self, acceleration):
        """
        Update current acceleration. Check are already made before.

        :param acceleration:
        :return:
        """
        for idx, a in enumerate(acceleration):
            if a is not None and idx in self._active_axis_hw_index:
                self._interface.gCode(f'M201 ' + self._idx_to_letter(idx) + str(a))
        return True

    def _set_speed(self, speed):
        """
        Set the target speed in mm/s

        :param speed:
        :return:
        """
        for idx, s in enumerate(speed):
            if s is not None and idx in self._active_axis_hw_index:
                # save temporary speed, will be used while moving only
                self._temp_speed_mm_min[idx] = max([0.5, s]) * 60  # 0.5 mm/s is the minimum allowed speed in RepRap Firmware
        return True

    def _get_axis_state(self, index=None) -> List:  # todo improve it > self._interface.getStatus() could gives more precise info than that.
        states = [Axis.State.UNKNOWN] * self.max_motors_count

        global_status, homing_status = self._interface.getStatus()

        for i in self._active_axis_hw_index:
            self._homed[i] = (homing_status[i] != 0)  # TODO temp fix: send info via Axis object instead

            if homing_status[i] == 0:
                states[i] = Axis.State.UNKNOWN
            elif global_status != 'idle':
                states[i] = Axis.State.MOVING
            else:
                states[i] = Axis.State.READY
        return states
