#  Copyright (c) 2022. Droptimize

import logging
import time

import requests
import json
import sys

logger = logging.getLogger(__name__)


class DuetWebAPI:
    def __init__(self, base_url, password):
        self._base_url = base_url
        self._password = password
        self.pt = 0
        self._session_timeout = 0.0
        self._last_request = None

    def _request(self, command, retries=5):
        """
        Send a request to the printer API, try to reconnect if authentifaction fails
        :param command:
        :return:
        """
        url = f'{self._base_url}/{command}'

        if self._last_request and self._session_timeout and (time.time() - self._last_request) > self._session_timeout:
            self._connect()

        try:  # TODO retry should not be necessary if the session timeout is well managed by the code above
            while retries > 0:
                r = requests.get(url, timeout=(2, 60))
                if r.status_code == 200:  # TODO check also r.ok?
                    return json.loads(r.text)
                elif r.status_code == 401:
                    self._connect()
                    retries -= 1
                else:
                    logger.warning(f"Unexpected http response: {r}")
                    retries -= 1
        except:
            logger.warning("Failed to get the request response")
        return None

    def _connect(self):
        try:
            url = (f'{self._base_url}/rr_connect?password={self._password}')
            r = requests.get(url, timeout=(2, 60))
            j = json.loads(r.text)  # TODO read the timeout we received and
            if j['err'] != 0:
                logger.error(f"Error while connecting to API: {j['err']}")
            self._session_timeout = j['sessionTimeout']
            self._last_request = time.time()
            return True

        except:
            logger.error(f"Error while connecting to API.")
            return

    def connect(self):
        """
        Connect to the API and check the firmware version

        :return:
        """
        self._connect()

        try:
            url = (f'{self._base_url}/rr_status?type=1')
            r = requests.get(url, timeout=(2, 60))
            j = json.loads(r.text)
            _ = j['coords']
            self.pt = 2
            return True
        except:
            try:
                url = (f'{self._base_url}' + '/machine/status')
                r = requests.get(url, timeout=(2, 60))
                j = json.loads(r.text)
                _ = j['result']
                self.pt = 3
                return True
            except:
                print(self._base_url, " does not appear to be a RRF2 or RRF3 printer", file=sys.stderr)
                return False

    # The following methods are a more atomic, reading/writing basic data structures in the printer.
    ####

    def printerType(self):
        return (self.pt)

    def baseURL(self):
        return (self._base_url)

    def getCoords(self):
        if self.pt == 2:
            j = self._request('rr_status?type=2')
            if j is None:
                return None
            jc = j['coords']['xyz']
            an = j['axisNames']
            ret = json.loads('{}')
            for i in range(0, len(jc)):
                ret[an[i]] = jc[i]
            return ret
        if self.pt == 3:
            j = self._request('machine/status')
            if j is None:
                return None
            ja = j['result']['move']['axes']
            # d=j['result']['move']['drives']
            # ad=self.json.loads('{}')
            # for i in range(0,len(ja)):
            #    ad[ ja[i]['letter'] ] = ja[i]['drives'][0]
            ret = json.loads('{}')
            for i in range(0, len(ja)):
                ret[ja[i]['letter']] = ja[i]['userPosition']
            return ret

    def getLayer(self):
        if self.pt == 2:
            j = self._request('rr_status?type=3')
            if j is None:
                return None
            s = j['currentLayer']
            return s
        if self.pt == 3:
            j = self._request('machine/status')
            if j is None:
                return None
            s = j['result']['job']['layer']
            if s == None: s = 0
            return s

    def getG10ToolOffset(self, tool):
        if (self.pt == 3):
            j = self._request('machine/status')
            if j is None:
                return None
            ja = j['result']['move']['axes']
            jt = j['result']['tools']
            ret = json.loads('{}')
            to = jt[tool]['offsets']
            for i in range(0, len(to)):
                ret[ja[i]['letter']] = to[i]
            return (ret)
        return ({'X': 0, 'Y': 0, 'Z': 0})  # Dummy for now

    def getNumExtruders(self):
        if (self.pt == 2):
            j = self._request('rr_status?type=2')
            if j is None:
                return None
            jc = j['coords']['extr']
            return (len(jc))
        if (self.pt == 3):
            j = self._request('machine/status')
            if j is None:
                return None
            return (len(j['result']['move']['extruders']))

    def getNumTools(self):
        if (self.pt == 2):
            j = self._request('rr_status?type=2')
            if j is None:
                return None
            jc = j['tools']
            return (len(jc))
        if (self.pt == 3):
            j = self._request('machine/status')
            if j is None:
                return None
            return (len(j['result']['tools']))

    def getStatus(self):  #todo draft : to be clean/completed
        if (self.pt == 2):
            j = self._request('rr_status?type=2')
            if j is None:
                return None
            global_status = j['status']
            if ('I' in global_status):
                global_status = ('idle')
            if ('P' in global_status):
                global_status = ('processing')
            if ('S' in global_status):
                global_status = ('paused')
            if ('B' in global_status):
                global_status = ('canceling')

            homing_status = j['coords']['axesHomed']

            return (global_status, homing_status)
        if (self.pt == 3):
            j = self._request('machine/status')
            if j is None:
                return None
            return (j['result']['state']['status'], None)


    def gCode(self, command):
        if (self.pt == 2):
            j = self._request('rr_gcode?gcode=' + command)
            if j is None:
                return None
            return True
        if (self.pt == 3):
            # TODO implement
            return False

    # def getFilenamed(self, filename):
    #     if self.pt == 2:
    #         url = (f'{self._base_url}' + '/rr_download?name=' + filename)
    #     if self.pt == 3:
    #         url = (f'{self._base_url}' + '/machine/file/' + filename)
    #     r = self.requests.get(url)
    #     return r.text.splitlines()  # replace('\n',str(chr(0x0a))).replace('\t','    '))
    #
    # def getTemperatures(self):
    #     if self.pt == 2:
    #         URL = (f'{self._base_url}' + '/rr_status?type=2')
    #         r = self.requests.get(URL)
    #         j = self.json.loads(r.text)
    #         return ('Error: getTemperatures not implemented (yet) for RRF V2 printers.')
    #     if self.pt == 3:
    #         URL = (f'{self._base_url}' + '/machine/status')
    #         r = self.requests.get(URL)
    #         j = self.json.loads(r.text)
    #         jsa = j['result']['sensors']['analog']
    #         return jsa
    #
    # ####
    # # The following methods provide services built on the atomics above.
    # ####
    #
    # # Given a line from config g that defines an endstop (N574) or Z probe (M558),
    # # Return a line that will define the same thing to a "nil" pin, i.e. undefine it
    # def _nilEndstop(self, configLine):
    #     ret = ''
    #     for each in [word for word in configLine.split()]:
    #         ret = ret + (each if (not (('P' in each[0]) or ('p' in each[0]))) else 'P"nil"') + ' '
    #     return ret
    #
    # def clearEndstops(self):
    #     c = self.getFilenamed('/sys/config.g')
    #     for each in [line for line in c if (('M574 ' in line) or ('M558 ' in line))]:
    #         self.gCode(self._nilEndstop(each))
    #
    # def resetEndstops(self):
    #     c = self.getFilenamed('/sys/config.g')
    #     for each in [line for line in c if (('M574 ' in line) or ('M558 ' in line))]:
    #         self.gCode(self._nilEndstop(each))
    #     for each in [line for line in c if (('M574 ' in line) or ('M558 ' in line) or ('G31 ' in line))]:
    #         self.gCode(each)
    #
    # def resetAxisLimits(self):
    #     c = self.getFilenamed('/sys/config.g')
    #     for each in [line for line in c if 'M208 ' in line]:
    #         self.gCode(each)
    #
    # def resetG10(self):
    #     c = self.getFilenamed('/sys/config.g')
    #     for each in [line for line in c if 'G10 ' in line]:
    #         self.gCode(each)
