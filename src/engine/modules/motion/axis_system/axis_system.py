
#  Copyright (c) 2022. Droptimize

# motion can get the status of the axis (position, moving, ...), set/get axis configuration through axis interface (set/get limits, etc.)
#   for any movement, motion is sending general message to all axis system. Each AxisSystem manage it's own axis set.
#   motion can read status on AxisSystem as well (ready, ...)

import logging
import threading
import time
from abc import ABC, abstractmethod
from collections import namedtuple
from enum import Enum
from typing import Any, List, Dict

from engine.common.config_manager import ConfigManager
from engine.common.coordinates import Coordinate
from engine.modules.motion.axis.axis import Axis

logger = logging.getLogger(__name__)


class AxisSystem(ABC):
    """
    Manage an axis system. This is the interface to the HW controlling one or more axis.
    """
    CONFIG_PATH = 'axis_system'

    POLLING_BASE_PERIOD = 0.05

    _axis_system_count = 0

    def __init__(self):
        super().__init__()
        AxisSystem._axis_system_count += 1

        self._config = ConfigManager(self.CONFIG_PATH)
        self._config.add_required_key(['enable', 'polling_period'])

        self._axis = Coordinate(None, None, None)  # mapping Coordinate system -> Axis instance of this AxisSystem
        self._secondary_axis = {}  # dict of secondary axis (key = axis letter)

        self._axis_index = {} # mapping Axis -> hw_index
        self._inv_axis_index = {}  # mapping hw_index -> Axis

        self._is_moving_listener = None

        # worker thread for monitoring axis state/position
        self._worker_stop_request = threading.Event()
        self._worker = None
        self._hw_lock = threading.RLock()

    @property
    @abstractmethod
    def supported_axis_classes(self):
        raise NotImplementedError("Please return a list of all supported AxisClass here")

    @property
    @abstractmethod
    def max_motors_count(self):
        raise NotImplementedError("Please return the maximum number of motors to link to a single AxisSystem")

    def set_moving_observer(self, moving_observer):
        self._is_moving_listener = moving_observer

    def set_config(self, config_manager, root_key):
        if not self._config.set_config_from_parent(config_manager, root_key):
            logger.error("Error while setting axis_system config")
            return False

        self._axis = Coordinate(None, None, None)  # clean previous axis
        self._secondary_axis = {}  # clean previous axis

        self._axis_index = {}
        self._inv_axis_index = {}

        # implement all axis from config
        for idx, axis in enumerate(self._config.get_property(['axis'])):  # iterate over axis list
            found = False
            for axis_class in self.supported_axis_classes:  # all available axis
                if axis_class.CONFIG_PATH in axis.keys():
                    found = True

                    if axis[axis_class.CONFIG_PATH]['enable'] is True:
                        # instantiate axis
                        inst: Axis = axis_class(self)
                        inst.set_config(self._config, ['axis', idx, axis_class.CONFIG_PATH])

                        logger.info(f"Instantiate {inst.axis_letter}-axis {type(inst).__name__} in axis system {type(self).__name__}")

                        # link axis to axis_system
                        from engine.modules.motion.motion import Motion
                        if inst.axis_letter == 'x':  # todo make axis list from enumerate (in Coordinate class?) -> not dynamic/elegant
                            self._axis.x = inst
                        elif inst.axis_letter == 'y':
                            self._axis.y = inst
                        elif inst.axis_letter == 'z':
                            self._axis.z = inst

                        elif inst.axis_letter in [e.value for e in Motion.SecondaryAxis]:  # list all secondary axis here
                            self._secondary_axis[inst.axis_letter] = inst
                        else:
                            logger.error(f"Unknown axis letter: {inst.axis_letter}")

                        # link axis to hw output
                        if 0 <= inst.hw_index < self.max_motors_count:
                            self._axis_index[inst] = inst.hw_index
                        else:
                            logger.error("Invalid hw_index while instantiating axis")

                        self._inv_axis_index[inst.hw_index] = inst

                        break
            if not found:
                logger.error(f"Error while instantiating axis: {axis} not supported here, ignoring it")

        return self._configure()  # configure system-specific options

    @abstractmethod
    def _configure(self):
        """
        Configure system-specific features
        :return:
        """
        raise NotImplementedError("Please implement")

    @property
    def axis(self) -> Coordinate:
        """
        Return the axis of the current axis_system

        :return: Axis if implemented, None otherwise
        """
        return self._axis

    @property
    def secondary_axis(self) -> Dict[str, Axis]:
        """
        Return the axis of the current axis_system

        :return: Axis if implemented, None otherwise
        """
        return self._secondary_axis

    @property
    def _active_axis_hw_index(self):
        """
        List of hw_index where a motor is connected
        :return:
        """
        active = []
        for a in self._axis:
            if a is not None:
                active.append(self._axis_index[a])
        return active

    def connect(self):
        """
        Connect to the axis system.

        :return:
        """
        with self._hw_lock:
            status = self._connect()

        if status:
            self._worker = threading.Thread(target=self._polling_loop, name=f'AxisSystem_{self._axis_system_count-1:2d}_{self.CONFIG_PATH}')
            self._worker.start()

        self.__update()

        return status

    def disconnect(self):
        """
        Disconnect from HW

        :return:
        """
        if self._worker and self._worker.is_alive():
            self._worker_stop_request.set()  # kill worker thread
            self._worker.join()
            self._worker_stop_request.clear()

        return self._disconnect()

    def move_to(self, position: Coordinate, speed: Coordinate = Coordinate(None, None, None)):
        """
        Move to the target position and set the optional speed.
        Returns as soon as the axis is moving

        :param position:
        :param speed:
        :return: True is movement is valid, False otherwise
        """
        if any([a.is_moving() for a in self._axis if a is not None]):
            logger.error(f"Cannot start a new movement while moving, ignoring command ({[a.is_moving() for a in self._axis if a is not None]})")
            return False

        self.set_speed(speed)

        target_pos = [None] * self.max_motors_count
        for p, s, axis in zip(position, speed, self._axis):  # type: Any, Any, Axis
            if axis is None or p is None:
                continue

            axis.check_if_position_valid(p)
        #    if s is not None:
        #       axis.check_if_speed_valid(s)

            target_pos[self._axis_index[axis]] = axis.axis_limits.position_max - p if axis.is_inverted else p

        if all(p is None for p in target_pos):  # no movement
            return True

        with self._hw_lock:
            status = self._move_to(target_pos)

        self.__update()

        return status

    def move_secondary_axis_to(self, axis_name, position: float, speed: float):
        """
        Move to the specified axis.
        Returns as soon as the axis is moving

        :param position:
        :param speed:
        :return: True is movement is valid, False otherwise
        """
        if axis_name not in self._secondary_axis:
            logger.debug(f"{axis_name} axis not implemented (at least not in the axis system, ignoring...")
            return True

        if self._secondary_axis[axis_name].is_moving():
            logger.error(f"Cannot start a new movement while moving, ignoring command...")
            return False

        if any([a.is_moving() for a in self._axis if a is not None]):  # primary axis already moving, not allowed within same axis system
            logger.error("Cannot start a new movement while primary axis already moving, not allowed within same axis system, ignoring...")
            return False

        axis = self._secondary_axis[axis_name]

        axis.check_if_position_valid(position)

        # self.set_speed(speed)   # TODO implement speed and acceleration!!!

        target_pos = [None] * self.max_motors_count
        target_pos[self._axis_index[axis]] = axis.axis_limits.position_max - position if axis.is_inverted else position

        if all(p is None for p in target_pos):  # no movement
            return True

        with self._hw_lock:
            status = self._move_to(target_pos)

        self.__update()

        return status

    def set_position(self, position: Coordinate):
        """
        Set the current position to a certain value

        :param position: Coordinate with position in mm or None
        :return: True is position is valid and taken as new current position, False otherwise
        """
        if any([a.is_moving() for a in self._axis if a is not None]):
            logger.error("Cannot set position while moving, ignoring command")
            return False

        target_pos = [None] * self.max_motors_count
        for axis, p in zip(self._axis, position):
            if axis is None or p is None:
                continue

            axis.check_if_position_valid(p)

            target_pos[self._axis_index[axis]] = axis.axis_limits.position_max - p if axis.is_inverted else p

        with self._hw_lock:
            res = self._set_position(target_pos)

        self.__update()
        return res

    def set_speed(self, speed: Coordinate):
        """
        Set the current speed to a certain value

        :param speed: Coordinate with speed in mm/s or None
        :return: True is speed is valid and taken as new current speed, False otherwise
        """
        if any([a.is_moving() for a in self._axis if a is not None]):
            logger.error("Cannot set speed while moving, ignoring command")
            return False

        target_speed = [None] * self.max_motors_count
        for axis, s in zip(self._axis, speed):
            if axis is None or s is None:
                continue

            axis.check_if_speed_valid(s)

            target_speed[self._axis_index[axis]] = s

        with self._hw_lock:
            res = self._set_speed(target_speed)

        self.__update()
        return res

    def set_acceleration(self, acceleration: Coordinate):
        """
        Set the current acceleration to a certain value

        :param acceleration: Coordinate with acceleration in mm/s^2 or None
        :return: True is acceleration is valid and taken as new current acceleration, False otherwise
        """
        if any([a.is_moving() for a in self._axis if a is not None]):
            logger.error("Cannot set acceleration while moving, ignoring command")
            return False

        target_acceleration = [None] * self.max_motors_count
        for axis, s in zip(self._axis, acceleration):
            if axis is None or s is None:
                continue

            axis.check_if_acceleration_valid(s)

            target_acceleration[self._axis_index[axis]] = s

        with self._hw_lock:
            res = self._set_acceleration(target_acceleration)

        self.__update()
        return res

    def calibrate(self, force=False):
        """
        Calibrate axis (most of the time, by homing)

        :param force: if set, force calibration even if axis is already calibrated
        :return:
        """
        self.__update()
        cal = [False] * self.max_motors_count
        for axis in self._axis:
            if axis is not None:
                cal[self._axis_index[axis]] = True
        state = self._calibrate(cal, force)
        self.__update()
        return state


    # HW related methods:

    @abstractmethod
    def _connect(self):
        raise NotImplementedError("Please implement")

    @abstractmethod
    def _disconnect(self):
        raise NotImplementedError("Please Implement this method")

    def stop(self):
        """
        Abort current movement if any

        :return: True is successfully stopped, false otherwise
        """
        status = self._stop()
        self.__update()
        return status

    @abstractmethod
    def _stop(self):
        """
        Abort current movement if any

        :return: True is successfully stopped, false otherwise
        """
        raise NotImplementedError("Please implement this method")

    @abstractmethod
    def set_axis_power(self, enable):  # todo not really needed?
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _get_position(self, index=None) -> List:
        """
        Return positions of all axis (list of hw_index positions)
        :param index: output index. If None, return all index.
        :return: Always a list of self._MAX_MOTORS_COUNT with None if the position was not requested or invalid
        """
        raise NotImplementedError("Please implement this method")

    @abstractmethod
    def _get_axis_state(self, index=None) -> List:
        """
        Return positions of all axis (list of hw_index positions)
        :param index: output index. If None, return all index.
        :return: Always a list of self._MAX_MOTORS_COUNT with None if the position was not requested or invalid
        """
        raise NotImplementedError("Please implement this method")

    @abstractmethod
    def _set_position(self, position: List):
        """
        Set the axis positions. If None, do not set (if not possible to set individual axis, it's up to the axis system to read the current position and update all axis at once.
        :param position: list of self._MAX_MOTORS_COUNT length of position to set, or with position = None.
        :return:
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _set_speed(self, speed: List):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _set_acceleration(self, acceleration: List):
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _move_to(self, position: List):
        """
        Move to the target position, return as soon as the axis is moving.
        This method is also responsible to update the state of the axis (ie. when MOVING, etc.)

        :param position: list of position, with position in mm or None if no movement required.
        :return: True if the movement is valid as axis started to move, False otherwise
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def _calibrate(self, axis, force=False):
        """
        Start calibration procedure: reset axis, do homing, returns directly

        :param axis: list of flag to indicate which axis to calibrate
        :param force: force calibration even if axis has its calibrated flag set
        :return: True if calibration has started
        """
        raise NotImplementedError("Please Implement this method")

    @abstractmethod
    def emergency_stop(self):
        """
        Stop immediately and return

        :return:
        """
        raise NotImplementedError

    def _polling_loop(self):
        """
        Monitor HW and cache position in Axis objects to speed-up read access

        :return:
        """
        polling_period = self._config.get_property(['polling_period'])
        current_period = 0.0

        while not self._worker_stop_request.is_set():
            if current_period > self.POLLING_BASE_PERIOD:
                current_period -= self.POLLING_BASE_PERIOD
                time.sleep(self.POLLING_BASE_PERIOD)
            else:
                current_period = polling_period
                self.__update()  # update position and state

        return True

    def __update(self):
        """
        Update all cached values. Should be reentrant.
        :return:
        """
        with self._hw_lock:
            is_moving_changed = False
            pos_changed = False
            any_moving = False

            # update all properties of all Axis:
            for idx, (p, s) in enumerate(zip(self._get_position(), self._get_axis_state())):
                if idx in self._active_axis_hw_index:
                    new_pos = self._inv_axis_index[idx].axis_limits.position_max - p if self._inv_axis_index[idx].is_inverted else p
                    if self._inv_axis_index[idx].position != new_pos:
                        self._inv_axis_index[idx].position = new_pos
                        pos_changed = True

                    if self._inv_axis_index[idx].state != s:
                        is_moving_changed = True
                        any_moving |= (s == Axis.State.MOVING)
                        self._inv_axis_index[idx].state = s

            # if any of the axis changes is state, notify main module
            if is_moving_changed or (not any_moving and pos_changed):
                # change of moving state or position while not moving (too small movement)
                if self._is_moving_listener:
                    self._is_moving_listener()
