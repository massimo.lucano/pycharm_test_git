#  Copyright (c) 2022. Droptimize

import threading
import time
from queue import Queue, Empty

import logging

from engine.modules.motion.axis.axis import Axis
from engine.modules.motion.axis_system.axis_system import AxisSystem

from engine.modules.motion.axis.sim_axis import SimAxis

logger = logging.getLogger(__name__)


class SimAxisSystem(AxisSystem):

    CONFIG_PATH = 'sim_system'

    _MAX_MOTORS_COUNT = 4

    _THREAD_PERIOD_S = 0.1

    def __init__(self):
        super().__init__()

        self._connected = False

        # HW emulated values
        self._current_acceleration = [100.0] * self._MAX_MOTORS_COUNT
        self._current_speed = [30.0] * self._MAX_MOTORS_COUNT  # absolute value
        self._current_position = [0.0] * self._MAX_MOTORS_COUNT
        self._state = [Axis.State.UNKNOWN] * self._MAX_MOTORS_COUNT
        self._calibrated = [False] * self._MAX_MOTORS_COUNT

        # worker thread emulating an axis
        self._stop_request = threading.Event()
        self._estop_request = threading.Event()
        self._pos_lock = threading.Lock()
        self._motion_request_queue = Queue()

        self._worker = None

    @property
    def supported_axis_classes(self):
        return [SimAxis]

    @property
    def max_motors_count(self):
        return self._MAX_MOTORS_COUNT

    def _configure(self):
        return True

    def _connect(self):
        self._worker = threading.Thread(target=self.worker_thread,
                                        name=f'SimAxisSystem_worker_') # {self._config.get_property("axis")}
        self._worker.daemon = True  # todo it seems that the thread is not terminating properly - set as daemon for now
        self._worker.start()
        self._connected = True
        return True

    def _disconnect(self):
        if self._worker and self._worker.is_alive():
            self._stop_request.set()  # kill worker thread
            self._worker.join()
            self._stop_request.clear()
        self._estop_request.clear()
        self._worker = None
        self._connected = False
        return True

    def _stop(self):
        self._motion_request_queue.put([None] * self._MAX_MOTORS_COUNT)
        return True

    def set_axis_power(self, enable):
        pass

    def _set_position(self, position):
        for idx, p in enumerate(position):
            if p is not None:
                with self._pos_lock:
                    self._current_position[idx] = p
        return True

    def _get_position(self, index=None):
        with self._pos_lock:
            return self._current_position.copy()

    def _get_axis_state(self, index=None):
        with self._pos_lock:
            return self._state.copy()

    def _set_speed(self, speed):
        for idx, s in enumerate(speed):
            if s is not None:
                with self._pos_lock:
                    self._current_speed[idx] = s
        return True

    def _set_acceleration(self, acceleration):
        for idx, a in enumerate(acceleration):
            if a is not None:
                with self._pos_lock:
                    self._current_acceleration[idx] = a
        return True

    def _move_to(self, position):
        logger.debug(f"Sim axis system: move to :{position}")
        target_pos = [None] * self._MAX_MOTORS_COUNT

        for idx, pos in enumerate(position):
            if pos is not None:
                target_pos[idx] = pos

        self._motion_request_queue.put(target_pos)

        polling = self._THREAD_PERIOD_S*2
        timeout_secs = 2

        while timeout_secs > 0:
            time.sleep(polling)
            timeout_secs -= polling

            with self._pos_lock:
                if all([self._state[idx] == Axis.State.MOVING or (p == self._current_position[idx]) for idx, p in enumerate(target_pos) if p is not None]):  # todo add some tolerances?
                    return True

        # set moving state for all axis which are moving soon...
        for idx, p in enumerate(position):
            if p is not None:
                self._state[idx] = Axis.State.MOVING

        logger.error("Timeout while waiting start of movement")
        return False

    def _calibrate(self, axis, force=False):
        for idx, a in enumerate(axis):
            if a is True:
                self._state[idx] = Axis.State.READY
        return True

    def emergency_stop(self):
        self.stop()

    def worker_thread(self):
        """
        Separate thread emulating all movements and updating the position
        :return:
        """
        target_position = [None] * self._MAX_MOTORS_COUNT

        while not self._stop_request.is_set():
            if self._estop_request.is_set():
                target_position = [None] * self._MAX_MOTORS_COUNT
                continue

            start = time.time()

            with self._pos_lock:
                # current speed
                step_per_period = [s * self._THREAD_PERIOD_S for s in self._current_speed]

                # read new command if any
                if not self._motion_request_queue.empty():  # and all([p is None for p in target_position]) is True:
                    try:
                        target_position = self._motion_request_queue.get_nowait()
                        logger.debug(f'New movement started to {target_position}')
                    except Empty:
                        target_position = [None] * self._MAX_MOTORS_COUNT

                # execute movement if any
                for idx, pos in enumerate(target_position):
                    if pos is not None:  # and self._state[idx] == Axis.State.READY:
                        # check end of movement
                        if -step_per_period[idx] < (self._current_position[idx] - target_position[idx]) < +step_per_period[idx]:
                            self._current_position[idx] = target_position[idx]
                            target_position[idx] = None
                            logger.debug(f'End of movement (axis: {idx})')
                            self._state[idx] = Axis.State.READY
                        else:
                            if target_position[idx] > self._current_position[idx]:
                                self._current_position[idx] += step_per_period[idx]
                            else:
                                self._current_position[idx] -= step_per_period[idx]

                            # logger.debug(f'Position:{self._current_position}')
                            self._state[idx] = Axis.State.MOVING
                    else:
                        self._state[idx] = Axis.State.READY

            elapsed = time.time() - start
            time.sleep(max(self._THREAD_PERIOD_S - elapsed, 0))
        return True
