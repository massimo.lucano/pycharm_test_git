Usage
=====

.. _installation:

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
Installation
------------

To use lib_axis_system, add the dependency in your **project.toml** file:

.. code-block:: python 
	:linenos:
	:emphasize-lines: 3
	
	dependencies = [
		#iPrint Packages
		'lib-axis-system',
		'duetwebapi-fix',
		'logger-module',
		#Public Packages
		'pyyaml'			
	]
	
   
Examples
--------

1st. Create axis system and read coordinates, read status and update configuration from YAML file

.. code-block:: python 
	:linenos:
	
	import os
	import sys
	from logger_module.axis_logger import logger

	from axis_abstract_classes import axis_abstract_classes
	from LIB_config_parser import config_parser_duet_2

	local_file_name = 'AxisConfig.yaml'
	file_path = os.path.realpath(__file__)
	dir_path = os.path.dirname(file_path)
	config_file_path = os.path.join(dir_path,'..','src','axis_abstract_classes', 'duet', local_file_name)

	#Generate logger instances
	logger_axis_instance = logger(logger_name = "axis_logger", log_file_path = "general.log") #Init logger for axis_module
	logger_config_parser_instance = logger(logger_name = "config_parser_logger", log_file_path = "general.log") #Init logger for axis_module

	# Parse configuration file
	config = config_parser_duet_2(config_file_path, logger_config_parser_instance)
	#Generate axis system and initialize it
	axis = axis_abstract_classes(config = config, logger = logger_axis_instance)

	#Example of use of the functions
	print(axis.get_coords())
	print(axis.get_status())

	#Read file again and send parameters to board
	axis.write_config_to_board(file_output = True)