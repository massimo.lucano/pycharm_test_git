API Reference
=============

.. _reference/axis_abstract_classes.rst:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::

   reference/axis_abstract_classes.rst
   reference/duet_2.rst 

To use Lumache, first install it using pip:

.. code-block:: console

  (.venv) $ pip install lumache
   
