# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here.
# import pathlib
# import os,sys
# path_top = os.path.join(pathlib.Path(__file__).parents[2],'src', 'axis_abstract_classes')
# print(path_top)
# sys.path.insert(0, path_top)

# import axis_abstract_classes, LIB_config_parser
# import axis_abstract_classes.duet.HAL_duet_2

from sphinx_pyproject import SphinxConfig
config = SphinxConfig("../../pyproject.toml", globalns=globals())


#axis_abstract_classes.axis_abstract_classes()
# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output


