############
DUET 2 Board
############

.. toctree::
   :maxdepth: 2
   :caption: Contents:

LIB_axis_system_duet_2
----------------------
.. automodule:: axis_abstract_classes.duet.LIB_axis_system_duet_2
  :members:
  :show-inheritance:
  :inherited-members:
  :special-members:

LIB_axis_duet_2
---------------
.. automodule:: axis_abstract_classes.duet.LIB_axis_duet_2
  :members:
  :show-inheritance:
  :inherited-members:
  :special-members:
  
HAL_duet_2
----------
.. automodule:: axis_abstract_classes.duet.HAL_duet_2
  :members:
  :show-inheritance:
  :inherited-members:
  :special-members: