.. test_python_package documentation master file, created by
   sphinx-quickstart on Tue Aug 16 07:54:49 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lib_axis_system's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Introduction text

.. figure:: 1_Layers.png
   :align: center
   :scale: 75 %
   :alt: Layers structure in lib_aixs_system

   This is the caption of the figure (a simple paragraph).
   
Contents
--------

.. toctree::

   usage 
   api_reference   
      
Check out the :doc:`usage` section for further information.

Check out the :doc:`usage` section for further information, including how to
:ref:`install <installation>` the project.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`